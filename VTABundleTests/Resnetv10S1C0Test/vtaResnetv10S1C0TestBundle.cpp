

#include "vta/runtime.h"
#include "VTABundle.h"
#include <time.h>
#include <iostream>
#include <fstream>
#include "vtaResnetv10S1C0TestMainEntry.h"
SymbolTableEntry symbolTableEntry_vtaResnetv10S1C0TestBundle[2]={{"inputP",0,200704,'1'},{"outP",200704,200704,'1'}};
BundleConfig vtaResnetv10S1C0TestBundle_config = {37120, 401408, 0, 64, 2, symbolTableEntry_vtaResnetv10S1C0TestBundle};
int8_t* filterP;
int8_t* biasP;
extern VTACommandHandle vtaCmdH;

void vtaResnetv10S1C0TestMainEntry_load_module(uint8_t *constantWeight){
  filterP = (int8_t *)VTABufferAlloc(36864);
  VTABufferCopy((int8_t *)(constantWeight + 0), 0, filterP, 0, 36864, 1);
  biasP = (int8_t *)VTABufferAlloc(256);
  VTABufferCopy((int8_t *)(constantWeight + 36864), 0, biasP, 0, 256, 1);
}

void vtaResnetv10S1C0TestMainEntry_destroy_module(){
  VTABufferFree(filterP);
  VTABufferFree(biasP);
}
int vtaResnetv10S1C0TestMainEntry(uint8_t *constantWeight, uint8_t *mutableWeight, uint8_t *activations){

  //Run convolution : conv
  int8_t* inputP = (int8_t*)mutableWeight + 0;
  int8_t* outP = (int8_t*)mutableWeight + 200704;
  int8_t* conv_input_transpose = (int8_t *)VTABufferAlloc(200704);
  transpose_nhwc2vtaio(inputP, (int8_t* )VTABufferGetVirtAddr(conv_input_transpose), 1, 56, 56, 64);
  int8_t* conv_output_bef_transpose = (int8_t *)VTABufferAlloc(200704);
  convolution_wo_tr(conv_input_transpose, filterP, (int32_t *)biasP, conv_output_bef_transpose, 1, 56, 56, 64, 64, 3, 3, 1, 1, 0, 1, 6, 56, 56, vtaCmdH);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(conv_output_bef_transpose), outP, 1, 56, 56, 64 );
  VTABufferFree(conv_input_transpose);
  VTABufferFree(conv_output_bef_transpose);
  return 0;
}