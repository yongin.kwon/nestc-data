

#include "VTABundle.h"
#include "vta/runtime.h"
#include <fstream>
#include <iostream>
#include <time.h>
#include "vgg16_cifar10.h"
SymbolTableEntry symbolTableEntry_vgg16_cifar10[2]={{"input",0,3072,'1'},{"output",12288,10,'1'}};
BundleConfig vgg16_cifar10_config = {14996712, 12328, 0, 64, 2, symbolTableEntry_vgg16_cifar10};
namespace namespace_vgg16_cifar10 {
int8_t* A139__1;
int8_t* A140;
int8_t* A142__1;
int8_t* A143;
int8_t* A145__1;
int8_t* A146;
int8_t* A148__1;
int8_t* A149;
int8_t* A151__1;
int8_t* A152;
int8_t* A154__1;
int8_t* A155;
int8_t* A157__1;
int8_t* A158;
int8_t* A160__1;
int8_t* A161;
int8_t* A163__1;
int8_t* A164;
int8_t* A166__1;
int8_t* A167;
int8_t* A169__1;
int8_t* A170;
int8_t* A172__1;
int8_t* A173;
int8_t* A175__1;
int8_t* A176;
int8_t* classifier_0_weight__1;
int8_t* classifier_0_bias;
int8_t* classifier_2_weight__1;
int8_t* classifier_2_bias;
}
using namespace namespace_vgg16_cifar10;
extern VTACommandHandle vtaCmdH;

void vgg16_cifar10_load_module(uint8_t *constantWeight){
  A139__1 = (int8_t *)VTABufferAlloc(1728);
  VTABufferCopy((int8_t *)(constantWeight + 0), 0, A139__1, 0, 1728, 1);
  A140 = (int8_t *)VTABufferAlloc(256);
  VTABufferCopy((int8_t *)(constantWeight + 1728), 0, A140, 0, 256, 1);
  A142__1 = (int8_t *)VTABufferAlloc(36864);
  VTABufferCopy((int8_t *)(constantWeight + 1984), 0, A142__1, 0, 36864, 1);
  A143 = (int8_t *)VTABufferAlloc(256);
  VTABufferCopy((int8_t *)(constantWeight + 38848), 0, A143, 0, 256, 1);
  A145__1 = (int8_t *)VTABufferAlloc(73728);
  VTABufferCopy((int8_t *)(constantWeight + 39104), 0, A145__1, 0, 73728, 1);
  A146 = (int8_t *)VTABufferAlloc(512);
  VTABufferCopy((int8_t *)(constantWeight + 112832), 0, A146, 0, 512, 1);
  A148__1 = (int8_t *)VTABufferAlloc(147456);
  VTABufferCopy((int8_t *)(constantWeight + 113344), 0, A148__1, 0, 147456, 1);
  A149 = (int8_t *)VTABufferAlloc(512);
  VTABufferCopy((int8_t *)(constantWeight + 260800), 0, A149, 0, 512, 1);
  A151__1 = (int8_t *)VTABufferAlloc(294912);
  VTABufferCopy((int8_t *)(constantWeight + 261312), 0, A151__1, 0, 294912, 1);
  A152 = (int8_t *)VTABufferAlloc(1024);
  VTABufferCopy((int8_t *)(constantWeight + 556224), 0, A152, 0, 1024, 1);
  A154__1 = (int8_t *)VTABufferAlloc(589824);
  VTABufferCopy((int8_t *)(constantWeight + 557248), 0, A154__1, 0, 589824, 1);
  A155 = (int8_t *)VTABufferAlloc(1024);
  VTABufferCopy((int8_t *)(constantWeight + 1147072), 0, A155, 0, 1024, 1);
  A157__1 = (int8_t *)VTABufferAlloc(589824);
  VTABufferCopy((int8_t *)(constantWeight + 1148096), 0, A157__1, 0, 589824, 1);
  A158 = (int8_t *)VTABufferAlloc(1024);
  VTABufferCopy((int8_t *)(constantWeight + 1737920), 0, A158, 0, 1024, 1);
  A160__1 = (int8_t *)VTABufferAlloc(1179648);
  VTABufferCopy((int8_t *)(constantWeight + 1738944), 0, A160__1, 0, 1179648, 1);
  A161 = (int8_t *)VTABufferAlloc(2048);
  VTABufferCopy((int8_t *)(constantWeight + 2918592), 0, A161, 0, 2048, 1);
  A163__1 = (int8_t *)VTABufferAlloc(2359296);
  VTABufferCopy((int8_t *)(constantWeight + 2920640), 0, A163__1, 0, 2359296, 1);
  A164 = (int8_t *)VTABufferAlloc(2048);
  VTABufferCopy((int8_t *)(constantWeight + 5279936), 0, A164, 0, 2048, 1);
  A166__1 = (int8_t *)VTABufferAlloc(2359296);
  VTABufferCopy((int8_t *)(constantWeight + 5281984), 0, A166__1, 0, 2359296, 1);
  A167 = (int8_t *)VTABufferAlloc(2048);
  VTABufferCopy((int8_t *)(constantWeight + 7641280), 0, A167, 0, 2048, 1);
  A169__1 = (int8_t *)VTABufferAlloc(2359296);
  VTABufferCopy((int8_t *)(constantWeight + 7643328), 0, A169__1, 0, 2359296, 1);
  A170 = (int8_t *)VTABufferAlloc(2048);
  VTABufferCopy((int8_t *)(constantWeight + 10002624), 0, A170, 0, 2048, 1);
  A172__1 = (int8_t *)VTABufferAlloc(2359296);
  VTABufferCopy((int8_t *)(constantWeight + 10004672), 0, A172__1, 0, 2359296, 1);
  A173 = (int8_t *)VTABufferAlloc(2048);
  VTABufferCopy((int8_t *)(constantWeight + 12363968), 0, A173, 0, 2048, 1);
  A175__1 = (int8_t *)VTABufferAlloc(2359296);
  VTABufferCopy((int8_t *)(constantWeight + 12366016), 0, A175__1, 0, 2359296, 1);
  A176 = (int8_t *)VTABufferAlloc(2048);
  VTABufferCopy((int8_t *)(constantWeight + 14725312), 0, A176, 0, 2048, 1);
  classifier_0_weight__1 = (int8_t *)VTABufferAlloc(262144);
  VTABufferCopy((int8_t *)(constantWeight + 14727360), 0, classifier_0_weight__1, 0, 262144, 1);
  classifier_0_bias = (int8_t *)VTABufferAlloc(2048);
  VTABufferCopy((int8_t *)(constantWeight + 14989504), 0, classifier_0_bias, 0, 2048, 1);
  classifier_2_weight__1 = (int8_t *)VTABufferAlloc(5120);
  VTABufferCopy((int8_t *)(constantWeight + 14991552), 0, classifier_2_weight__1, 0, 5120, 1);
  classifier_2_bias = (int8_t *)VTABufferAlloc(40);
  VTABufferCopy((int8_t *)(constantWeight + 14996672), 0, classifier_2_bias, 0, 40, 1);
}

void vgg16_cifar10_destroy_module(){
  VTABufferFree(A139__1);
  VTABufferFree(A140);
  VTABufferFree(A142__1);
  VTABufferFree(A143);
  VTABufferFree(A145__1);
  VTABufferFree(A146);
  VTABufferFree(A148__1);
  VTABufferFree(A149);
  VTABufferFree(A151__1);
  VTABufferFree(A152);
  VTABufferFree(A154__1);
  VTABufferFree(A155);
  VTABufferFree(A157__1);
  VTABufferFree(A158);
  VTABufferFree(A160__1);
  VTABufferFree(A161);
  VTABufferFree(A163__1);
  VTABufferFree(A164);
  VTABufferFree(A166__1);
  VTABufferFree(A167);
  VTABufferFree(A169__1);
  VTABufferFree(A170);
  VTABufferFree(A172__1);
  VTABufferFree(A173);
  VTABufferFree(A175__1);
  VTABufferFree(A176);
  VTABufferFree(classifier_0_weight__1);
  VTABufferFree(classifier_0_bias);
  VTABufferFree(classifier_2_weight__1);
  VTABufferFree(classifier_2_bias);
}
int vgg16_cifar10(uint8_t *constantWeight, uint8_t *mutableWeight, uint8_t *activations){

  //Run allocactivation : Conv_0__1_quantize_res
  int8_t *Conv_0__1_quantize_res = (int8_t *)malloc(3072);

  //Run debugprint : debug_print.before.Conv_0__1_quantize.Src.quantize
  int8_t* input = (int8_t*)mutableWeight + 0;
  debugprint(input, 3072, "./debug/data0001.bin", 0);

  //Run quantize : Conv_0__1_quantize
  quantize(input, Conv_0__1_quantize_res, 3072, 1/128.000000, 0 );

  //Run debugprint : debug_print.after.Conv_0__1_quantize.Dest.quantize
  debugprint(Conv_0__1_quantize_res, 3072, "./debug/data0000.bin", 1);

  //Run allocactivation : Conv_0__9_res
  int8_t *Conv_0__9_res = (int8_t *)malloc(3072);

  //Run debugprint : debug_print.before.Conv_0__9.Src.transpose
  debugprint(Conv_0__1_quantize_res, 3072, "./debug/data0003.bin", 1);

  //Run transpose : Conv_0__9
  transpose(Conv_0__1_quantize_res, Conv_0__9_res, 1, 3, 32, 32, 1, 32, 32, 3, 0, 2, 3, 1 );

  //Run debugprint : debug_print.after.Conv_0__9.Dest.transpose
  debugprint(Conv_0__9_res, 3072, "./debug/data0002.bin", 1);

  //Run deallocactivation : dealloc_Conv_0__1_quantize_res
  free(Conv_0__1_quantize_res);

  //Run allocactivation : Conv_0__2_res
  int8_t *Conv_0__2_res = (int8_t *)malloc(65536);

  //Run debugprint : debug_print.before.Conv_0__2.Src.convolution
  debugprint(Conv_0__9_res, 3072, "./debug/data0005.bin", 1);

  //Run debugprint : debug_print.before.Conv_0__2.Filter.convolution
  debugprint((int8_t *)VTABufferGetVirtAddr(A139__1), 1728, "./debug/data0006.bin", 1);

  //Run debugprint : debug_print.before.Conv_0__2.Bias.convolution
  debugprint((int8_t *)VTABufferGetVirtAddr(A140), 64, "./debug/data0007.bin", 0);

  //Run convolution : Conv_0__2
  nonvtaconvolution(Conv_0__9_res, 1.0/128.000000, 0, (int8_t *)VTABufferGetVirtAddr(A139__1), 1.0/128.000000, 0, (int8_t *)VTABufferGetVirtAddr(A140), 1.0/16384.000000, 0, Conv_0__2_res, 1.0/64.000000, 0, 1, 32, 32, 3, 64, 3, 3, 1, 1, 1, 1, 1, 1, 32, 32 );

  //Run debugprint : debug_print.after.Conv_0__2.Dest.convolution
  debugprint(Conv_0__2_res, 65536, "./debug/data0004.bin", 1);

  //Run deallocactivation : dealloc_Conv_0__9_res
  free(Conv_0__9_res);

  //Run allocactivation : Conv_2__2_res
  int8_t *Conv_2__2_res = (int8_t *)malloc(65536);

  //Run debugprint : debug_print.before.Conv_2__2.Src.convolution
  debugprint(Conv_0__2_res, 65536, "./debug/data0009.bin", 1);

  //Run debugprint : debug_print.before.Conv_2__2.Filter.convolution
  debugprint((int8_t *)VTABufferGetVirtAddr(A142__1), 36864, "./debug/data0010.bin", 1);

  //Run debugprint : debug_print.before.Conv_2__2.Bias.convolution
  debugprint((int8_t *)VTABufferGetVirtAddr(A143), 64, "./debug/data0011.bin", 0);

  //Run convolution : Conv_2__2
  int8_t* Conv_2__2_input_transpose = (int8_t *)VTABufferAlloc(65536);
  transpose_nhwc2vtaio(Conv_0__2_res, (int8_t* )VTABufferGetVirtAddr(Conv_2__2_input_transpose), 1, 32, 32, 64);
  int8_t* Conv_2__2_output_bef_transpose = (int8_t *)VTABufferAlloc(65536);
  convolution_wo_tr(Conv_2__2_input_transpose, A142__1, (int32_t *)A143, Conv_2__2_output_bef_transpose, 1, 32, 32, 64, 64, 3, 3, 1, 1, 1, 1, 8, 32, 32, vtaCmdH, 1, 14, 14, 0);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(Conv_2__2_output_bef_transpose), Conv_2__2_res, 1, 32, 32, 64 );
  VTABufferFree(Conv_2__2_input_transpose);
  VTABufferFree(Conv_2__2_output_bef_transpose);

  //Run debugprint : debug_print.after.Conv_2__2.Dest.convolution
  debugprint(Conv_2__2_res, 65536, "./debug/data0008.bin", 1);

  //Run deallocactivation : dealloc_Conv_0__2_res
  free(Conv_0__2_res);

  //Run allocactivation : MaxPool_4__1_res
  int8_t *MaxPool_4__1_res = (int8_t *)malloc(16384);

  //Run debugprint : debug_print.before.MaxPool_4__2.Src.maxpool
  debugprint(Conv_2__2_res, 65536, "./debug/data0013.bin", 1);

  //Run maxpool : MaxPool_4__2
  maxpool(Conv_2__2_res, MaxPool_4__1_res, 1, 32, 32, 64, 2, 2, 0, 2 );

  //Run debugprint : debug_print.after.MaxPool_4__2.Dest.maxpool
  debugprint(MaxPool_4__1_res, 16384, "./debug/data0012.bin", 1);

  //Run deallocactivation : dealloc_Conv_2__2_res
  free(Conv_2__2_res);

  //Run allocactivation : Conv_5__2_res
  int8_t *Conv_5__2_res = (int8_t *)malloc(32768);

  //Run debugprint : debug_print.before.Conv_5__2.Src.convolution
  debugprint(MaxPool_4__1_res, 16384, "./debug/data0015.bin", 1);

  //Run debugprint : debug_print.before.Conv_5__2.Filter.convolution
  debugprint((int8_t *)VTABufferGetVirtAddr(A145__1), 73728, "./debug/data0016.bin", 1);

  //Run debugprint : debug_print.before.Conv_5__2.Bias.convolution
  debugprint((int8_t *)VTABufferGetVirtAddr(A146), 128, "./debug/data0017.bin", 0);

  //Run convolution : Conv_5__2
  int8_t* Conv_5__2_input_transpose = (int8_t *)VTABufferAlloc(16384);
  transpose_nhwc2vtaio(MaxPool_4__1_res, (int8_t* )VTABufferGetVirtAddr(Conv_5__2_input_transpose), 1, 16, 16, 64);
  int8_t* Conv_5__2_output_bef_transpose = (int8_t *)VTABufferAlloc(32768);
  convolution_wo_tr(Conv_5__2_input_transpose, A145__1, (int32_t *)A146, Conv_5__2_output_bef_transpose, 1, 16, 16, 64, 128, 3, 3, 1, 1, 1, 1, 9, 16, 16, vtaCmdH, 1, 14, 14, 0);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(Conv_5__2_output_bef_transpose), Conv_5__2_res, 1, 16, 16, 128 );
  VTABufferFree(Conv_5__2_input_transpose);
  VTABufferFree(Conv_5__2_output_bef_transpose);

  //Run debugprint : debug_print.after.Conv_5__2.Dest.convolution
  debugprint(Conv_5__2_res, 32768, "./debug/data0014.bin", 1);

  //Run deallocactivation : dealloc_MaxPool_4__1_res
  free(MaxPool_4__1_res);

  //Run allocactivation : Conv_7__2_res
  int8_t *Conv_7__2_res = (int8_t *)malloc(32768);

  //Run debugprint : debug_print.before.Conv_7__2.Src.convolution
  debugprint(Conv_5__2_res, 32768, "./debug/data0019.bin", 1);

  //Run debugprint : debug_print.before.Conv_7__2.Filter.convolution
  debugprint((int8_t *)VTABufferGetVirtAddr(A148__1), 147456, "./debug/data0020.bin", 1);

  //Run debugprint : debug_print.before.Conv_7__2.Bias.convolution
  debugprint((int8_t *)VTABufferGetVirtAddr(A149), 128, "./debug/data0021.bin", 0);

  //Run convolution : Conv_7__2
  int8_t* Conv_7__2_input_transpose = (int8_t *)VTABufferAlloc(32768);
  transpose_nhwc2vtaio(Conv_5__2_res, (int8_t* )VTABufferGetVirtAddr(Conv_7__2_input_transpose), 1, 16, 16, 128);
  int8_t* Conv_7__2_output_bef_transpose = (int8_t *)VTABufferAlloc(32768);
  convolution_wo_tr(Conv_7__2_input_transpose, A148__1, (int32_t *)A149, Conv_7__2_output_bef_transpose, 1, 16, 16, 128, 128, 3, 3, 1, 1, 1, 1, 10, 16, 16, vtaCmdH, 1, 14, 14, 0);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(Conv_7__2_output_bef_transpose), Conv_7__2_res, 1, 16, 16, 128 );
  VTABufferFree(Conv_7__2_input_transpose);
  VTABufferFree(Conv_7__2_output_bef_transpose);

  //Run debugprint : debug_print.after.Conv_7__2.Dest.convolution
  debugprint(Conv_7__2_res, 32768, "./debug/data0018.bin", 1);

  //Run deallocactivation : dealloc_Conv_5__2_res
  free(Conv_5__2_res);

  //Run allocactivation : MaxPool_9__1_res
  int8_t *MaxPool_9__1_res = (int8_t *)malloc(8192);

  //Run debugprint : debug_print.before.MaxPool_9__2.Src.maxpool
  debugprint(Conv_7__2_res, 32768, "./debug/data0023.bin", 1);

  //Run maxpool : MaxPool_9__2
  maxpool(Conv_7__2_res, MaxPool_9__1_res, 1, 16, 16, 128, 2, 2, 0, 2 );

  //Run debugprint : debug_print.after.MaxPool_9__2.Dest.maxpool
  debugprint(MaxPool_9__1_res, 8192, "./debug/data0022.bin", 1);

  //Run deallocactivation : dealloc_Conv_7__2_res
  free(Conv_7__2_res);

  //Run allocactivation : Conv_10__2_res
  int8_t *Conv_10__2_res = (int8_t *)malloc(16384);

  //Run debugprint : debug_print.before.Conv_10__2.Src.convolution
  debugprint(MaxPool_9__1_res, 8192, "./debug/data0025.bin", 1);

  //Run debugprint : debug_print.before.Conv_10__2.Filter.convolution
  debugprint((int8_t *)VTABufferGetVirtAddr(A151__1), 294912, "./debug/data0026.bin", 1);

  //Run debugprint : debug_print.before.Conv_10__2.Bias.convolution
  debugprint((int8_t *)VTABufferGetVirtAddr(A152), 256, "./debug/data0027.bin", 0);

  //Run convolution : Conv_10__2
  int8_t* Conv_10__2_input_transpose = (int8_t *)VTABufferAlloc(8192);
  transpose_nhwc2vtaio(MaxPool_9__1_res, (int8_t* )VTABufferGetVirtAddr(Conv_10__2_input_transpose), 1, 8, 8, 128);
  int8_t* Conv_10__2_output_bef_transpose = (int8_t *)VTABufferAlloc(16384);
  convolution_wo_tr(Conv_10__2_input_transpose, A151__1, (int32_t *)A152, Conv_10__2_output_bef_transpose, 1, 8, 8, 128, 256, 3, 3, 1, 1, 1, 1, 9, 8, 8, vtaCmdH, 1, 14, 14, 0);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(Conv_10__2_output_bef_transpose), Conv_10__2_res, 1, 8, 8, 256 );
  VTABufferFree(Conv_10__2_input_transpose);
  VTABufferFree(Conv_10__2_output_bef_transpose);

  //Run debugprint : debug_print.after.Conv_10__2.Dest.convolution
  debugprint(Conv_10__2_res, 16384, "./debug/data0024.bin", 1);

  //Run deallocactivation : dealloc_MaxPool_9__1_res
  free(MaxPool_9__1_res);

  //Run allocactivation : Conv_12__2_res
  int8_t *Conv_12__2_res = (int8_t *)malloc(16384);

  //Run debugprint : debug_print.before.Conv_12__2.Src.convolution
  debugprint(Conv_10__2_res, 16384, "./debug/data0029.bin", 1);

  //Run debugprint : debug_print.before.Conv_12__2.Filter.convolution
  debugprint((int8_t *)VTABufferGetVirtAddr(A154__1), 589824, "./debug/data0030.bin", 1);

  //Run debugprint : debug_print.before.Conv_12__2.Bias.convolution
  debugprint((int8_t *)VTABufferGetVirtAddr(A155), 256, "./debug/data0031.bin", 0);

  //Run convolution : Conv_12__2
  int8_t* Conv_12__2_input_transpose = (int8_t *)VTABufferAlloc(16384);
  transpose_nhwc2vtaio(Conv_10__2_res, (int8_t* )VTABufferGetVirtAddr(Conv_12__2_input_transpose), 1, 8, 8, 256);
  int8_t* Conv_12__2_output_bef_transpose = (int8_t *)VTABufferAlloc(16384);
  convolution_wo_tr(Conv_12__2_input_transpose, A154__1, (int32_t *)A155, Conv_12__2_output_bef_transpose, 1, 8, 8, 256, 256, 3, 3, 1, 1, 1, 1, 9, 8, 8, vtaCmdH, 1, 14, 14, 0);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(Conv_12__2_output_bef_transpose), Conv_12__2_res, 1, 8, 8, 256 );
  VTABufferFree(Conv_12__2_input_transpose);
  VTABufferFree(Conv_12__2_output_bef_transpose);

  //Run debugprint : debug_print.after.Conv_12__2.Dest.convolution
  debugprint(Conv_12__2_res, 16384, "./debug/data0028.bin", 1);

  //Run deallocactivation : dealloc_Conv_10__2_res
  free(Conv_10__2_res);

  //Run allocactivation : Conv_14__2_res
  int8_t *Conv_14__2_res = (int8_t *)malloc(16384);

  //Run debugprint : debug_print.before.Conv_14__2.Src.convolution
  debugprint(Conv_12__2_res, 16384, "./debug/data0033.bin", 1);

  //Run debugprint : debug_print.before.Conv_14__2.Filter.convolution
  debugprint((int8_t *)VTABufferGetVirtAddr(A157__1), 589824, "./debug/data0034.bin", 1);

  //Run debugprint : debug_print.before.Conv_14__2.Bias.convolution
  debugprint((int8_t *)VTABufferGetVirtAddr(A158), 256, "./debug/data0035.bin", 0);

  //Run convolution : Conv_14__2
  int8_t* Conv_14__2_input_transpose = (int8_t *)VTABufferAlloc(16384);
  transpose_nhwc2vtaio(Conv_12__2_res, (int8_t* )VTABufferGetVirtAddr(Conv_14__2_input_transpose), 1, 8, 8, 256);
  int8_t* Conv_14__2_output_bef_transpose = (int8_t *)VTABufferAlloc(16384);
  convolution_wo_tr(Conv_14__2_input_transpose, A157__1, (int32_t *)A158, Conv_14__2_output_bef_transpose, 1, 8, 8, 256, 256, 3, 3, 1, 1, 1, 1, 9, 8, 8, vtaCmdH, 1, 14, 14, 0);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(Conv_14__2_output_bef_transpose), Conv_14__2_res, 1, 8, 8, 256 );
  VTABufferFree(Conv_14__2_input_transpose);
  VTABufferFree(Conv_14__2_output_bef_transpose);

  //Run debugprint : debug_print.after.Conv_14__2.Dest.convolution
  debugprint(Conv_14__2_res, 16384, "./debug/data0032.bin", 1);

  //Run deallocactivation : dealloc_Conv_12__2_res
  free(Conv_12__2_res);

  //Run allocactivation : MaxPool_16__1_res
  int8_t *MaxPool_16__1_res = (int8_t *)malloc(4096);

  //Run debugprint : debug_print.before.MaxPool_16__2.Src.maxpool
  debugprint(Conv_14__2_res, 16384, "./debug/data0037.bin", 1);

  //Run maxpool : MaxPool_16__2
  maxpool(Conv_14__2_res, MaxPool_16__1_res, 1, 8, 8, 256, 2, 2, 0, 2 );

  //Run debugprint : debug_print.after.MaxPool_16__2.Dest.maxpool
  debugprint(MaxPool_16__1_res, 4096, "./debug/data0036.bin", 1);

  //Run deallocactivation : dealloc_Conv_14__2_res
  free(Conv_14__2_res);

  //Run allocactivation : Conv_17__2_res
  int8_t *Conv_17__2_res = (int8_t *)malloc(8192);

  //Run debugprint : debug_print.before.Conv_17__2.Src.convolution
  debugprint(MaxPool_16__1_res, 4096, "./debug/data0039.bin", 1);

  //Run debugprint : debug_print.before.Conv_17__2.Filter.convolution
  debugprint((int8_t *)VTABufferGetVirtAddr(A160__1), 1179648, "./debug/data0040.bin", 1);

  //Run debugprint : debug_print.before.Conv_17__2.Bias.convolution
  debugprint((int8_t *)VTABufferGetVirtAddr(A161), 512, "./debug/data0041.bin", 0);

  //Run convolution : Conv_17__2
  int8_t* Conv_17__2_input_transpose = (int8_t *)VTABufferAlloc(4096);
  transpose_nhwc2vtaio(MaxPool_16__1_res, (int8_t* )VTABufferGetVirtAddr(Conv_17__2_input_transpose), 1, 4, 4, 256);
  int8_t* Conv_17__2_output_bef_transpose = (int8_t *)VTABufferAlloc(8192);
  convolution_wo_tr(Conv_17__2_input_transpose, A160__1, (int32_t *)A161, Conv_17__2_output_bef_transpose, 1, 4, 4, 256, 512, 3, 3, 1, 1, 1, 1, 10, 4, 4, vtaCmdH, 1, 14, 14, 0);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(Conv_17__2_output_bef_transpose), Conv_17__2_res, 1, 4, 4, 512 );
  VTABufferFree(Conv_17__2_input_transpose);
  VTABufferFree(Conv_17__2_output_bef_transpose);

  //Run debugprint : debug_print.after.Conv_17__2.Dest.convolution
  debugprint(Conv_17__2_res, 8192, "./debug/data0038.bin", 1);

  //Run deallocactivation : dealloc_MaxPool_16__1_res
  free(MaxPool_16__1_res);

  //Run allocactivation : Conv_19__2_res
  int8_t *Conv_19__2_res = (int8_t *)malloc(8192);

  //Run debugprint : debug_print.before.Conv_19__2.Src.convolution
  debugprint(Conv_17__2_res, 8192, "./debug/data0043.bin", 1);

  //Run debugprint : debug_print.before.Conv_19__2.Filter.convolution
  debugprint((int8_t *)VTABufferGetVirtAddr(A163__1), 2359296, "./debug/data0044.bin", 1);

  //Run debugprint : debug_print.before.Conv_19__2.Bias.convolution
  debugprint((int8_t *)VTABufferGetVirtAddr(A164), 512, "./debug/data0045.bin", 0);

  //Run convolution : Conv_19__2
  int8_t* Conv_19__2_input_transpose = (int8_t *)VTABufferAlloc(8192);
  transpose_nhwc2vtaio(Conv_17__2_res, (int8_t* )VTABufferGetVirtAddr(Conv_19__2_input_transpose), 1, 4, 4, 512);
  int8_t* Conv_19__2_output_bef_transpose = (int8_t *)VTABufferAlloc(8192);
  convolution_wo_tr(Conv_19__2_input_transpose, A163__1, (int32_t *)A164, Conv_19__2_output_bef_transpose, 1, 4, 4, 512, 512, 3, 3, 1, 1, 1, 1, 10, 4, 4, vtaCmdH, 1, 14, 14, 0);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(Conv_19__2_output_bef_transpose), Conv_19__2_res, 1, 4, 4, 512 );
  VTABufferFree(Conv_19__2_input_transpose);
  VTABufferFree(Conv_19__2_output_bef_transpose);

  //Run debugprint : debug_print.after.Conv_19__2.Dest.convolution
  debugprint(Conv_19__2_res, 8192, "./debug/data0042.bin", 1);

  //Run deallocactivation : dealloc_Conv_17__2_res
  free(Conv_17__2_res);

  //Run allocactivation : Conv_21__2_res
  int8_t *Conv_21__2_res = (int8_t *)malloc(8192);

  //Run debugprint : debug_print.before.Conv_21__2.Src.convolution
  debugprint(Conv_19__2_res, 8192, "./debug/data0047.bin", 1);

  //Run debugprint : debug_print.before.Conv_21__2.Filter.convolution
  debugprint((int8_t *)VTABufferGetVirtAddr(A166__1), 2359296, "./debug/data0048.bin", 1);

  //Run debugprint : debug_print.before.Conv_21__2.Bias.convolution
  debugprint((int8_t *)VTABufferGetVirtAddr(A167), 512, "./debug/data0049.bin", 0);

  //Run convolution : Conv_21__2
  int8_t* Conv_21__2_input_transpose = (int8_t *)VTABufferAlloc(8192);
  transpose_nhwc2vtaio(Conv_19__2_res, (int8_t* )VTABufferGetVirtAddr(Conv_21__2_input_transpose), 1, 4, 4, 512);
  int8_t* Conv_21__2_output_bef_transpose = (int8_t *)VTABufferAlloc(8192);
  convolution_wo_tr(Conv_21__2_input_transpose, A166__1, (int32_t *)A167, Conv_21__2_output_bef_transpose, 1, 4, 4, 512, 512, 3, 3, 1, 1, 1, 1, 10, 4, 4, vtaCmdH, 1, 14, 14, 0);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(Conv_21__2_output_bef_transpose), Conv_21__2_res, 1, 4, 4, 512 );
  VTABufferFree(Conv_21__2_input_transpose);
  VTABufferFree(Conv_21__2_output_bef_transpose);

  //Run debugprint : debug_print.after.Conv_21__2.Dest.convolution
  debugprint(Conv_21__2_res, 8192, "./debug/data0046.bin", 1);

  //Run deallocactivation : dealloc_Conv_19__2_res
  free(Conv_19__2_res);

  //Run allocactivation : MaxPool_23__1_res
  int8_t *MaxPool_23__1_res = (int8_t *)malloc(2048);

  //Run debugprint : debug_print.before.MaxPool_23__2.Src.maxpool
  debugprint(Conv_21__2_res, 8192, "./debug/data0051.bin", 1);

  //Run maxpool : MaxPool_23__2
  maxpool(Conv_21__2_res, MaxPool_23__1_res, 1, 4, 4, 512, 2, 2, 0, 2 );

  //Run debugprint : debug_print.after.MaxPool_23__2.Dest.maxpool
  debugprint(MaxPool_23__1_res, 2048, "./debug/data0050.bin", 1);

  //Run deallocactivation : dealloc_Conv_21__2_res
  free(Conv_21__2_res);

  //Run allocactivation : Conv_24__2_res
  int8_t *Conv_24__2_res = (int8_t *)malloc(2048);

  //Run debugprint : debug_print.before.Conv_24__2.Src.convolution
  debugprint(MaxPool_23__1_res, 2048, "./debug/data0053.bin", 1);

  //Run debugprint : debug_print.before.Conv_24__2.Filter.convolution
  debugprint((int8_t *)VTABufferGetVirtAddr(A169__1), 2359296, "./debug/data0054.bin", 1);

  //Run debugprint : debug_print.before.Conv_24__2.Bias.convolution
  debugprint((int8_t *)VTABufferGetVirtAddr(A170), 512, "./debug/data0055.bin", 0);

  //Run convolution : Conv_24__2
  int8_t* Conv_24__2_input_transpose = (int8_t *)VTABufferAlloc(2048);
  transpose_nhwc2vtaio(MaxPool_23__1_res, (int8_t* )VTABufferGetVirtAddr(Conv_24__2_input_transpose), 1, 2, 2, 512);
  int8_t* Conv_24__2_output_bef_transpose = (int8_t *)VTABufferAlloc(2048);
  convolution_wo_tr(Conv_24__2_input_transpose, A169__1, (int32_t *)A170, Conv_24__2_output_bef_transpose, 1, 2, 2, 512, 512, 3, 3, 1, 1, 1, 1, 9, 2, 2, vtaCmdH, 1, 14, 14, 0);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(Conv_24__2_output_bef_transpose), Conv_24__2_res, 1, 2, 2, 512 );
  VTABufferFree(Conv_24__2_input_transpose);
  VTABufferFree(Conv_24__2_output_bef_transpose);

  //Run debugprint : debug_print.after.Conv_24__2.Dest.convolution
  debugprint(Conv_24__2_res, 2048, "./debug/data0052.bin", 1);

  //Run deallocactivation : dealloc_MaxPool_23__1_res
  free(MaxPool_23__1_res);

  //Run allocactivation : Conv_26__2_res
  int8_t *Conv_26__2_res = (int8_t *)malloc(2048);

  //Run debugprint : debug_print.before.Conv_26__2.Src.convolution
  debugprint(Conv_24__2_res, 2048, "./debug/data0057.bin", 1);

  //Run debugprint : debug_print.before.Conv_26__2.Filter.convolution
  debugprint((int8_t *)VTABufferGetVirtAddr(A172__1), 2359296, "./debug/data0058.bin", 1);

  //Run debugprint : debug_print.before.Conv_26__2.Bias.convolution
  debugprint((int8_t *)VTABufferGetVirtAddr(A173), 512, "./debug/data0059.bin", 0);

  //Run convolution : Conv_26__2
  int8_t* Conv_26__2_input_transpose = (int8_t *)VTABufferAlloc(2048);
  transpose_nhwc2vtaio(Conv_24__2_res, (int8_t* )VTABufferGetVirtAddr(Conv_26__2_input_transpose), 1, 2, 2, 512);
  int8_t* Conv_26__2_output_bef_transpose = (int8_t *)VTABufferAlloc(2048);
  convolution_wo_tr(Conv_26__2_input_transpose, A172__1, (int32_t *)A173, Conv_26__2_output_bef_transpose, 1, 2, 2, 512, 512, 3, 3, 1, 1, 1, 1, 9, 2, 2, vtaCmdH, 1, 14, 14, 0);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(Conv_26__2_output_bef_transpose), Conv_26__2_res, 1, 2, 2, 512 );
  VTABufferFree(Conv_26__2_input_transpose);
  VTABufferFree(Conv_26__2_output_bef_transpose);

  //Run debugprint : debug_print.after.Conv_26__2.Dest.convolution
  debugprint(Conv_26__2_res, 2048, "./debug/data0056.bin", 1);

  //Run deallocactivation : dealloc_Conv_24__2_res
  free(Conv_24__2_res);

  //Run allocactivation : Conv_28__2_res
  int8_t *Conv_28__2_res = (int8_t *)malloc(2048);

  //Run debugprint : debug_print.before.Conv_28__2.Src.convolution
  debugprint(Conv_26__2_res, 2048, "./debug/data0061.bin", 1);

  //Run debugprint : debug_print.before.Conv_28__2.Filter.convolution
  debugprint((int8_t *)VTABufferGetVirtAddr(A175__1), 2359296, "./debug/data0062.bin", 1);

  //Run debugprint : debug_print.before.Conv_28__2.Bias.convolution
  debugprint((int8_t *)VTABufferGetVirtAddr(A176), 512, "./debug/data0063.bin", 0);

  //Run convolution : Conv_28__2
  int8_t* Conv_28__2_input_transpose = (int8_t *)VTABufferAlloc(2048);
  transpose_nhwc2vtaio(Conv_26__2_res, (int8_t* )VTABufferGetVirtAddr(Conv_28__2_input_transpose), 1, 2, 2, 512);
  int8_t* Conv_28__2_output_bef_transpose = (int8_t *)VTABufferAlloc(2048);
  convolution_wo_tr(Conv_28__2_input_transpose, A175__1, (int32_t *)A176, Conv_28__2_output_bef_transpose, 1, 2, 2, 512, 512, 3, 3, 1, 1, 1, 1, 11, 2, 2, vtaCmdH, 1, 14, 14, 0);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(Conv_28__2_output_bef_transpose), Conv_28__2_res, 1, 2, 2, 512 );
  VTABufferFree(Conv_28__2_input_transpose);
  VTABufferFree(Conv_28__2_output_bef_transpose);

  //Run debugprint : debug_print.after.Conv_28__2.Dest.convolution
  debugprint(Conv_28__2_res, 2048, "./debug/data0060.bin", 1);

  //Run deallocactivation : dealloc_Conv_26__2_res
  free(Conv_26__2_res);

  //Run allocactivation : MaxPool_30__1_res
  int8_t *MaxPool_30__1_res = (int8_t *)malloc(512);

  //Run debugprint : debug_print.before.MaxPool_30__2.Src.maxpool
  debugprint(Conv_28__2_res, 2048, "./debug/data0065.bin", 1);

  //Run maxpool : MaxPool_30__2
  maxpool(Conv_28__2_res, MaxPool_30__1_res, 1, 2, 2, 512, 2, 2, 0, 2 );

  //Run debugprint : debug_print.after.MaxPool_30__2.Dest.maxpool
  debugprint(MaxPool_30__1_res, 512, "./debug/data0064.bin", 1);

  //Run deallocactivation : dealloc_Conv_28__2_res
  free(Conv_28__2_res);

  //Run tensorview : MaxPool_30__1_res__2
  int8_t* MaxPool_30__1_res__2 = MaxPool_30__1_res;

  //Run allocactivation : Gemm_37__1_res
  int8_t *Gemm_37__1_res = (int8_t *)malloc(512);

  //Run debugprint : debug_print.before.Gemm_37__1.Src.fullyconnected
  debugprint(MaxPool_30__1_res__2, 512, "./debug/data0067.bin", 1);

  //Run debugprint : debug_print.before.Gemm_37__1.Weights.fullyconnected
  debugprint((int8_t *)VTABufferGetVirtAddr(classifier_0_weight__1), 262144, "./debug/data0068.bin", 1);

  //Run debugprint : debug_print.before.Gemm_37__1.Bias.fullyconnected
  debugprint((int8_t *)VTABufferGetVirtAddr(classifier_0_bias), 512, "./debug/data0069.bin", 0);

  //Run fullyconnected : Gemm_37__1
  fullyconnected(MaxPool_30__1_res__2, 1.0/64.000000, 0, (int8_t *)VTABufferGetVirtAddr(classifier_0_weight__1), 1.0/1024.000000, 0, (int8_t *)VTABufferGetVirtAddr(classifier_0_bias), 1.0/65536.000000, 0, Gemm_37__1_res, 1.0/64.000000, 0, 1, 512, 512, 512, 1, 512, 1 );

  //Run debugprint : debug_print.after.Gemm_37__1.Dest.fullyconnected
  debugprint(Gemm_37__1_res, 512, "./debug/data0066.bin", 1);

  //Run deallocactivation : dealloc_MaxPool_30__1_res
  free(MaxPool_30__1_res);

  //Run debugprint : debug_print.before.Relu_38.Src.relu
  debugprint(Gemm_37__1_res, 512, "./debug/data0071.bin", 1);

  //Run relu : Relu_38
  relu(Gemm_37__1_res, Gemm_37__1_res, 512 );

  //Run debugprint : debug_print.after.Relu_38.Dest.relu
  debugprint(Gemm_37__1_res, 512, "./debug/data0070.bin", 1);

  //Run allocactivation : Gemm_39__1_res
  int8_t *Gemm_39__1_res = (int8_t *)malloc(10);

  //Run debugprint : debug_print.before.Gemm_39__1.Src.fullyconnected
  debugprint(Gemm_37__1_res, 512, "./debug/data0073.bin", 1);

  //Run debugprint : debug_print.before.Gemm_39__1.Weights.fullyconnected
  debugprint((int8_t *)VTABufferGetVirtAddr(classifier_2_weight__1), 5120, "./debug/data0074.bin", 1);

  //Run debugprint : debug_print.before.Gemm_39__1.Bias.fullyconnected
  debugprint((int8_t *)VTABufferGetVirtAddr(classifier_2_bias), 10, "./debug/data0075.bin", 0);

  //Run fullyconnected : Gemm_39__1
  fullyconnected(Gemm_37__1_res, 1.0/64.000000, 0, (int8_t *)VTABufferGetVirtAddr(classifier_2_weight__1), 1.0/256.000000, 0, (int8_t *)VTABufferGetVirtAddr(classifier_2_bias), 1.0/16384.000000, 0, Gemm_39__1_res, 1.0/16.000000, 0, 1, 512, 512, 10, 1, 10, 1 );

  //Run debugprint : debug_print.after.Gemm_39__1.Dest.fullyconnected
  debugprint(Gemm_39__1_res, 10, "./debug/data0072.bin", 1);

  //Run deallocactivation : dealloc_Gemm_37__1_res
  free(Gemm_37__1_res);

  //Run debugprint : debug_print.before.Gemm_39__1_dequantize.Src.dequantize
  debugprint(Gemm_39__1_res, 10, "./debug/data0077.bin", 1);

  //Run dequantize : Gemm_39__1_dequantize
  int8_t* output = (int8_t*)mutableWeight + 12288;
  dequantize(Gemm_39__1_res, output, 10, 1/16.000000, 0 );

  //Run debugprint : debug_print.after.Gemm_39__1_dequantize.Dest.dequantize
  debugprint(output, 10, "./debug/data0076.bin", 0);

  //Run deallocactivation : dealloc_Gemm_39__1_res
  free(Gemm_39__1_res);
  return 0;
}