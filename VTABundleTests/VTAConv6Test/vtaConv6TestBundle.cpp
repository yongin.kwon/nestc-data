

#include "vta/runtime.h"
#include "VTABundle.h"
#include <time.h>
#include <iostream>
#include <fstream>
#include "vtaConv6TestMainEntry.h"
SymbolTableEntry symbolTableEntry_vtaConv6TestBundle[2]={{"inputP",0,48400,'1'},{"outP",48400,193600,'1'}};
BundleConfig vtaConv6TestBundle_config = {1280, 242000, 0, 64, 2, symbolTableEntry_vtaConv6TestBundle};
int8_t* filterP;
int8_t* biasP;
extern VTACommandHandle vtaCmdH;

void vtaConv6TestMainEntry_load_module(uint8_t *constantWeight){
  filterP = (int8_t *)VTABufferAlloc(1024);
  VTABufferCopy((int8_t *)(constantWeight + 0), 0, filterP, 0, 1024, 1);
  biasP = (int8_t *)VTABufferAlloc(256);
  VTABufferCopy((int8_t *)(constantWeight + 1024), 0, biasP, 0, 256, 1);
}

void vtaConv6TestMainEntry_destroy_module(){
  VTABufferFree(filterP);
  VTABufferFree(biasP);
}
int vtaConv6TestMainEntry(uint8_t *constantWeight, uint8_t *mutableWeight, uint8_t *activations){

  //Run convolution : conv
  int8_t* inputP = (int8_t*)mutableWeight + 0;
  int8_t* outP = (int8_t*)mutableWeight + 48400;
  int8_t* conv_input_transpose = (int8_t *)VTABufferAlloc(48400);
  transpose_nhwc2vtaio(inputP, (int8_t* )VTABufferGetVirtAddr(conv_input_transpose), 1, 55, 55, 16);
  int8_t* conv_output_bef_transpose = (int8_t *)VTABufferAlloc(193600);
  convolution_wo_tr(conv_input_transpose, filterP, (int32_t *)biasP, conv_output_bef_transpose, 1, 55, 55, 16, 64, 1, 1, 0, 1, 0, 1, 6, 55, 55, vtaCmdH);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(conv_output_bef_transpose), outP, 1, 55, 55, 64 );
  VTABufferFree(conv_input_transpose);
  VTABufferFree(conv_output_bef_transpose);
  return 0;
}