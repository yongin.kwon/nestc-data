

#include "VTABundle.h"
#include "vta/runtime.h"
#include <fstream>
#include <iostream>
#include <time.h>
#include "BirealNet.h"
SymbolTableEntry symbolTableEntry_BirealNet[2]={{"input",0,150528,'1'},{"output",602112,1000,'1'}};
BundleConfig BirealNet_config = {2173600, 606112, 0, 64, 2, symbolTableEntry_BirealNet};
namespace namespace_BirealNet {
int8_t* A245__1;
int8_t* A246;
int8_t* A256__2;
int8_t* conv_bias__32;
int8_t* Conv_3__2_scalingfactor;
int8_t* A257__2;
int8_t* conv_bias__33;
int8_t* Conv_7__2_scalingfactor;
int8_t* A258__2;
int8_t* conv_bias__34;
int8_t* Conv_11__2_scalingfactor;
int8_t* A259__2;
int8_t* conv_bias__16;
int8_t* Conv_15__2_scalingfactor;
int8_t* A260__2;
int8_t* conv_bias__35;
int8_t* Conv_19__2_scalingfactor;
int8_t* A248__1;
int8_t* A249;
int8_t* A261__2;
int8_t* conv_bias__36;
int8_t* Conv_26__2_scalingfactor;
int8_t* A262__2;
int8_t* conv_bias__37;
int8_t* Conv_30__2_scalingfactor;
int8_t* A263__2;
int8_t* conv_bias__20;
int8_t* Conv_34__2_scalingfactor;
int8_t* A264__2;
int8_t* conv_bias__38;
int8_t* Conv_38__2_scalingfactor;
int8_t* A251__1;
int8_t* A252;
int8_t* A265__2;
int8_t* conv_bias__39;
int8_t* Conv_45__2_scalingfactor;
int8_t* A266__2;
int8_t* conv_bias__40;
int8_t* Conv_49__2_scalingfactor;
int8_t* A267__2;
int8_t* conv_bias__24;
int8_t* Conv_53__2_scalingfactor;
int8_t* A268__2;
int8_t* conv_bias__41;
int8_t* Conv_57__2_scalingfactor;
int8_t* A254__1;
int8_t* A255;
int8_t* A269__2;
int8_t* conv_bias__42;
int8_t* Conv_64__2_scalingfactor;
int8_t* A270__2;
int8_t* conv_bias__43;
int8_t* Conv_68__2_scalingfactor;
int8_t* A271__2;
int8_t* conv_bias__28;
int8_t* Conv_72__2_scalingfactor;
int8_t* fc_weight__1;
int8_t* fc_bias;
}
using namespace namespace_BirealNet;
extern VTACommandHandle vtaCmdH;

void BirealNet_load_module(uint8_t *constantWeight){
  A245__1 = (int8_t *)VTABufferAlloc(50176);
  VTABufferCopy((int8_t *)(constantWeight + 0), 0, A245__1, 0, 50176, 1);
  A246 = (int8_t *)VTABufferAlloc(256);
  VTABufferCopy((int8_t *)(constantWeight + 50176), 0, A246, 0, 256, 1);
  A256__2 = (int8_t *)VTABufferAlloc(9216);
  VTABufferCopy((int8_t *)(constantWeight + 50432), 0, A256__2, 0, 9216, 1);
  conv_bias__32 = (int8_t *)VTABufferAlloc(256);
  VTABufferCopy((int8_t *)(constantWeight + 59648), 0, conv_bias__32, 0, 256, 1);
  Conv_3__2_scalingfactor = (int8_t *)VTABufferAlloc(256);
  VTABufferCopy((int8_t *)(constantWeight + 59904), 0, Conv_3__2_scalingfactor, 0, 256, 1);
  A257__2 = (int8_t *)VTABufferAlloc(9216);
  VTABufferCopy((int8_t *)(constantWeight + 60160), 0, A257__2, 0, 9216, 1);
  conv_bias__33 = (int8_t *)VTABufferAlloc(256);
  VTABufferCopy((int8_t *)(constantWeight + 69376), 0, conv_bias__33, 0, 256, 1);
  Conv_7__2_scalingfactor = (int8_t *)VTABufferAlloc(256);
  VTABufferCopy((int8_t *)(constantWeight + 69632), 0, Conv_7__2_scalingfactor, 0, 256, 1);
  A258__2 = (int8_t *)VTABufferAlloc(9216);
  VTABufferCopy((int8_t *)(constantWeight + 69888), 0, A258__2, 0, 9216, 1);
  conv_bias__34 = (int8_t *)VTABufferAlloc(256);
  VTABufferCopy((int8_t *)(constantWeight + 79104), 0, conv_bias__34, 0, 256, 1);
  Conv_11__2_scalingfactor = (int8_t *)VTABufferAlloc(256);
  VTABufferCopy((int8_t *)(constantWeight + 79360), 0, Conv_11__2_scalingfactor, 0, 256, 1);
  A259__2 = (int8_t *)VTABufferAlloc(9216);
  VTABufferCopy((int8_t *)(constantWeight + 79616), 0, A259__2, 0, 9216, 1);
  conv_bias__16 = (int8_t *)VTABufferAlloc(256);
  VTABufferCopy((int8_t *)(constantWeight + 88832), 0, conv_bias__16, 0, 256, 1);
  Conv_15__2_scalingfactor = (int8_t *)VTABufferAlloc(256);
  VTABufferCopy((int8_t *)(constantWeight + 89088), 0, Conv_15__2_scalingfactor, 0, 256, 1);
  A260__2 = (int8_t *)VTABufferAlloc(18432);
  VTABufferCopy((int8_t *)(constantWeight + 89344), 0, A260__2, 0, 18432, 1);
  conv_bias__35 = (int8_t *)VTABufferAlloc(512);
  VTABufferCopy((int8_t *)(constantWeight + 107776), 0, conv_bias__35, 0, 512, 1);
  Conv_19__2_scalingfactor = (int8_t *)VTABufferAlloc(512);
  VTABufferCopy((int8_t *)(constantWeight + 108288), 0, Conv_19__2_scalingfactor, 0, 512, 1);
  A248__1 = (int8_t *)VTABufferAlloc(8192);
  VTABufferCopy((int8_t *)(constantWeight + 108800), 0, A248__1, 0, 8192, 1);
  A249 = (int8_t *)VTABufferAlloc(512);
  VTABufferCopy((int8_t *)(constantWeight + 116992), 0, A249, 0, 512, 1);
  A261__2 = (int8_t *)VTABufferAlloc(18432);
  VTABufferCopy((int8_t *)(constantWeight + 117504), 0, A261__2, 0, 18432, 1);
  conv_bias__36 = (int8_t *)VTABufferAlloc(512);
  VTABufferCopy((int8_t *)(constantWeight + 135936), 0, conv_bias__36, 0, 512, 1);
  Conv_26__2_scalingfactor = (int8_t *)VTABufferAlloc(512);
  VTABufferCopy((int8_t *)(constantWeight + 136448), 0, Conv_26__2_scalingfactor, 0, 512, 1);
  A262__2 = (int8_t *)VTABufferAlloc(18432);
  VTABufferCopy((int8_t *)(constantWeight + 136960), 0, A262__2, 0, 18432, 1);
  conv_bias__37 = (int8_t *)VTABufferAlloc(512);
  VTABufferCopy((int8_t *)(constantWeight + 155392), 0, conv_bias__37, 0, 512, 1);
  Conv_30__2_scalingfactor = (int8_t *)VTABufferAlloc(512);
  VTABufferCopy((int8_t *)(constantWeight + 155904), 0, Conv_30__2_scalingfactor, 0, 512, 1);
  A263__2 = (int8_t *)VTABufferAlloc(18432);
  VTABufferCopy((int8_t *)(constantWeight + 156416), 0, A263__2, 0, 18432, 1);
  conv_bias__20 = (int8_t *)VTABufferAlloc(512);
  VTABufferCopy((int8_t *)(constantWeight + 174848), 0, conv_bias__20, 0, 512, 1);
  Conv_34__2_scalingfactor = (int8_t *)VTABufferAlloc(512);
  VTABufferCopy((int8_t *)(constantWeight + 175360), 0, Conv_34__2_scalingfactor, 0, 512, 1);
  A264__2 = (int8_t *)VTABufferAlloc(36864);
  VTABufferCopy((int8_t *)(constantWeight + 175872), 0, A264__2, 0, 36864, 1);
  conv_bias__38 = (int8_t *)VTABufferAlloc(1024);
  VTABufferCopy((int8_t *)(constantWeight + 212736), 0, conv_bias__38, 0, 1024, 1);
  Conv_38__2_scalingfactor = (int8_t *)VTABufferAlloc(1024);
  VTABufferCopy((int8_t *)(constantWeight + 213760), 0, Conv_38__2_scalingfactor, 0, 1024, 1);
  A251__1 = (int8_t *)VTABufferAlloc(32768);
  VTABufferCopy((int8_t *)(constantWeight + 214784), 0, A251__1, 0, 32768, 1);
  A252 = (int8_t *)VTABufferAlloc(1024);
  VTABufferCopy((int8_t *)(constantWeight + 247552), 0, A252, 0, 1024, 1);
  A265__2 = (int8_t *)VTABufferAlloc(73728);
  VTABufferCopy((int8_t *)(constantWeight + 248576), 0, A265__2, 0, 73728, 1);
  conv_bias__39 = (int8_t *)VTABufferAlloc(1024);
  VTABufferCopy((int8_t *)(constantWeight + 322304), 0, conv_bias__39, 0, 1024, 1);
  Conv_45__2_scalingfactor = (int8_t *)VTABufferAlloc(1024);
  VTABufferCopy((int8_t *)(constantWeight + 323328), 0, Conv_45__2_scalingfactor, 0, 1024, 1);
  A266__2 = (int8_t *)VTABufferAlloc(73728);
  VTABufferCopy((int8_t *)(constantWeight + 324352), 0, A266__2, 0, 73728, 1);
  conv_bias__40 = (int8_t *)VTABufferAlloc(1024);
  VTABufferCopy((int8_t *)(constantWeight + 398080), 0, conv_bias__40, 0, 1024, 1);
  Conv_49__2_scalingfactor = (int8_t *)VTABufferAlloc(1024);
  VTABufferCopy((int8_t *)(constantWeight + 399104), 0, Conv_49__2_scalingfactor, 0, 1024, 1);
  A267__2 = (int8_t *)VTABufferAlloc(73728);
  VTABufferCopy((int8_t *)(constantWeight + 400128), 0, A267__2, 0, 73728, 1);
  conv_bias__24 = (int8_t *)VTABufferAlloc(1024);
  VTABufferCopy((int8_t *)(constantWeight + 473856), 0, conv_bias__24, 0, 1024, 1);
  Conv_53__2_scalingfactor = (int8_t *)VTABufferAlloc(1024);
  VTABufferCopy((int8_t *)(constantWeight + 474880), 0, Conv_53__2_scalingfactor, 0, 1024, 1);
  A268__2 = (int8_t *)VTABufferAlloc(147456);
  VTABufferCopy((int8_t *)(constantWeight + 475904), 0, A268__2, 0, 147456, 1);
  conv_bias__41 = (int8_t *)VTABufferAlloc(2048);
  VTABufferCopy((int8_t *)(constantWeight + 623360), 0, conv_bias__41, 0, 2048, 1);
  Conv_57__2_scalingfactor = (int8_t *)VTABufferAlloc(2048);
  VTABufferCopy((int8_t *)(constantWeight + 625408), 0, Conv_57__2_scalingfactor, 0, 2048, 1);
  A254__1 = (int8_t *)VTABufferAlloc(131072);
  VTABufferCopy((int8_t *)(constantWeight + 627456), 0, A254__1, 0, 131072, 1);
  A255 = (int8_t *)VTABufferAlloc(2048);
  VTABufferCopy((int8_t *)(constantWeight + 758528), 0, A255, 0, 2048, 1);
  A269__2 = (int8_t *)VTABufferAlloc(294912);
  VTABufferCopy((int8_t *)(constantWeight + 760576), 0, A269__2, 0, 294912, 1);
  conv_bias__42 = (int8_t *)VTABufferAlloc(2048);
  VTABufferCopy((int8_t *)(constantWeight + 1055488), 0, conv_bias__42, 0, 2048, 1);
  Conv_64__2_scalingfactor = (int8_t *)VTABufferAlloc(2048);
  VTABufferCopy((int8_t *)(constantWeight + 1057536), 0, Conv_64__2_scalingfactor, 0, 2048, 1);
  A270__2 = (int8_t *)VTABufferAlloc(294912);
  VTABufferCopy((int8_t *)(constantWeight + 1059584), 0, A270__2, 0, 294912, 1);
  conv_bias__43 = (int8_t *)VTABufferAlloc(2048);
  VTABufferCopy((int8_t *)(constantWeight + 1354496), 0, conv_bias__43, 0, 2048, 1);
  Conv_68__2_scalingfactor = (int8_t *)VTABufferAlloc(2048);
  VTABufferCopy((int8_t *)(constantWeight + 1356544), 0, Conv_68__2_scalingfactor, 0, 2048, 1);
  A271__2 = (int8_t *)VTABufferAlloc(294912);
  VTABufferCopy((int8_t *)(constantWeight + 1358592), 0, A271__2, 0, 294912, 1);
  conv_bias__28 = (int8_t *)VTABufferAlloc(2048);
  VTABufferCopy((int8_t *)(constantWeight + 1653504), 0, conv_bias__28, 0, 2048, 1);
  Conv_72__2_scalingfactor = (int8_t *)VTABufferAlloc(2048);
  VTABufferCopy((int8_t *)(constantWeight + 1655552), 0, Conv_72__2_scalingfactor, 0, 2048, 1);
  fc_weight__1 = (int8_t *)VTABufferAlloc(512000);
  VTABufferCopy((int8_t *)(constantWeight + 1657600), 0, fc_weight__1, 0, 512000, 1);
  fc_bias = (int8_t *)VTABufferAlloc(4000);
  VTABufferCopy((int8_t *)(constantWeight + 2169600), 0, fc_bias, 0, 4000, 1);
}

void BirealNet_destroy_module(){
  VTABufferFree(A245__1);
  VTABufferFree(A246);
  VTABufferFree(A256__2);
  VTABufferFree(conv_bias__32);
  VTABufferFree(Conv_3__2_scalingfactor);
  VTABufferFree(A257__2);
  VTABufferFree(conv_bias__33);
  VTABufferFree(Conv_7__2_scalingfactor);
  VTABufferFree(A258__2);
  VTABufferFree(conv_bias__34);
  VTABufferFree(Conv_11__2_scalingfactor);
  VTABufferFree(A259__2);
  VTABufferFree(conv_bias__16);
  VTABufferFree(Conv_15__2_scalingfactor);
  VTABufferFree(A260__2);
  VTABufferFree(conv_bias__35);
  VTABufferFree(Conv_19__2_scalingfactor);
  VTABufferFree(A248__1);
  VTABufferFree(A249);
  VTABufferFree(A261__2);
  VTABufferFree(conv_bias__36);
  VTABufferFree(Conv_26__2_scalingfactor);
  VTABufferFree(A262__2);
  VTABufferFree(conv_bias__37);
  VTABufferFree(Conv_30__2_scalingfactor);
  VTABufferFree(A263__2);
  VTABufferFree(conv_bias__20);
  VTABufferFree(Conv_34__2_scalingfactor);
  VTABufferFree(A264__2);
  VTABufferFree(conv_bias__38);
  VTABufferFree(Conv_38__2_scalingfactor);
  VTABufferFree(A251__1);
  VTABufferFree(A252);
  VTABufferFree(A265__2);
  VTABufferFree(conv_bias__39);
  VTABufferFree(Conv_45__2_scalingfactor);
  VTABufferFree(A266__2);
  VTABufferFree(conv_bias__40);
  VTABufferFree(Conv_49__2_scalingfactor);
  VTABufferFree(A267__2);
  VTABufferFree(conv_bias__24);
  VTABufferFree(Conv_53__2_scalingfactor);
  VTABufferFree(A268__2);
  VTABufferFree(conv_bias__41);
  VTABufferFree(Conv_57__2_scalingfactor);
  VTABufferFree(A254__1);
  VTABufferFree(A255);
  VTABufferFree(A269__2);
  VTABufferFree(conv_bias__42);
  VTABufferFree(Conv_64__2_scalingfactor);
  VTABufferFree(A270__2);
  VTABufferFree(conv_bias__43);
  VTABufferFree(Conv_68__2_scalingfactor);
  VTABufferFree(A271__2);
  VTABufferFree(conv_bias__28);
  VTABufferFree(Conv_72__2_scalingfactor);
  VTABufferFree(fc_weight__1);
  VTABufferFree(fc_bias);
}
int BirealNet(uint8_t *constantWeight, uint8_t *mutableWeight, uint8_t *activations){

  //Run allocactivation : Conv_0__1_quantize_res
  int8_t *Conv_0__1_quantize_res = (int8_t *)malloc(150528);

  //Run quantize : Conv_0__1_quantize
  int8_t* input = (int8_t*)mutableWeight + 0;
  quantize(input, Conv_0__1_quantize_res, 150528, 1/32.000000, 0 );

  //Run allocactivation : Conv_0__8_res
  int8_t *Conv_0__8_res = (int8_t *)malloc(150528);

  //Run transpose : Conv_0__8
  transpose(Conv_0__1_quantize_res, Conv_0__8_res, 1, 3, 224, 224, 1, 224, 224, 3, 0, 2, 3, 1 );

  //Run deallocactivation : dealloc_Conv_0__1_quantize_res
  free(Conv_0__1_quantize_res);

  //Run allocactivation : Conv_0__2_res
  int8_t *Conv_0__2_res = (int8_t *)malloc(802816);

  //Run convolution : Conv_0__2
  int8_t* Conv_0__2_input_transpose = (int8_t *)VTABufferAlloc(802816);
  transpose_nhwc2vtaio_zerofill(Conv_0__8_res, (int8_t* )VTABufferGetVirtAddr(Conv_0__2_input_transpose), 1, 224, 224, 16);
  int8_t* Conv_0__2_output_bef_transpose = (int8_t *)VTABufferAlloc(802816);
  convolution_wo_tr(Conv_0__2_input_transpose, A245__1, (int32_t *)A246, Conv_0__2_output_bef_transpose, 1, 224, 224, 16, 64, 7, 7, 3, 2, 0, 1, 8, 112, 112, vtaCmdH, 1, 8, 8, 0);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(Conv_0__2_output_bef_transpose), Conv_0__2_res, 1, 112, 112, 64 );
  VTABufferFree(Conv_0__2_input_transpose);
  VTABufferFree(Conv_0__2_output_bef_transpose);

  //Run deallocactivation : dealloc_Conv_0__8_res
  free(Conv_0__8_res);

  //Run allocactivation : Add_5__1_res
  int8_t *Add_5__1_res = (int8_t *)malloc(200704);

  //Run maxpool : MaxPool_1__2
  maxpool(Conv_0__2_res, Add_5__1_res, 1, 112, 112, 64, 3, 3, 1, 2 );

  //Run deallocactivation : dealloc_Conv_0__2_res
  free(Conv_0__2_res);

  //Run allocactivation : Sign_2__1_res
  int8_t *Sign_2__1_res = (int8_t *)malloc(200704);

  //Run elementsign : Sign_2__1
  elemsign(Add_5__1_res, Sign_2__1_res, 200704);
 
  //Run allocactivation : bnn_Conv_3__2_res
  int8_t *bnn_Conv_3__2_res = (int8_t *)malloc(200704);

  //Run bnnconvolution : bnn_Conv_3__2
  int8_t* bnn_Conv_3__2_input_transpose = (int8_t *)VTABufferAlloc(50176);
  transpose_nhwc2vtaio_pack_zerofill(Sign_2__1_res, (int8_t* )VTABufferGetVirtAddr(bnn_Conv_3__2_input_transpose), 1, 56, 56, 8);
  int8_t* bnn_Conv_3__2_output_bef_transpose = (int8_t *)VTABufferAlloc(200704);
  xp_convolution_wo_tr(bnn_Conv_3__2_input_transpose, A256__2, (int32_t *)conv_bias__32, (int32_t *)Conv_3__2_scalingfactor, bnn_Conv_3__2_output_bef_transpose, 1, 56, 56, 16, 64, 3, 3, 1, 1, 0, 1, 7, 56, 56, vtaCmdH, 2, 8, 56, 0, 1);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(bnn_Conv_3__2_output_bef_transpose), bnn_Conv_3__2_res, 1, 56, 56, 64 );
  VTABufferFree(bnn_Conv_3__2_input_transpose);
  VTABufferFree(bnn_Conv_3__2_output_bef_transpose);

  //Run deallocactivation : dealloc_Sign_2__1_res
  free(Sign_2__1_res);

  //Run elementadd : Add_5__1
  elemadd(bnn_Conv_3__2_res, 1.0/64.000000, 0, Add_5__1_res, 1.0/4.000000, 0, Add_5__1_res, 1.0/4.000000, 0, 200704 );

  //Run deallocactivation : dealloc_bnn_Conv_3__2_res
  free(bnn_Conv_3__2_res);

  //Run allocactivation : Sign_6__1_res
  int8_t *Sign_6__1_res = (int8_t *)malloc(200704);

  //Run elementsign : Sign_6__1
  elemsign(Add_5__1_res, Sign_6__1_res, 200704);
 
  //Run allocactivation : bnn_Conv_7__2_res
  int8_t *bnn_Conv_7__2_res = (int8_t *)malloc(200704);

  //Run bnnconvolution : bnn_Conv_7__2
  int8_t* bnn_Conv_7__2_input_transpose = (int8_t *)VTABufferAlloc(50176);
  transpose_nhwc2vtaio_pack_zerofill(Sign_6__1_res, (int8_t* )VTABufferGetVirtAddr(bnn_Conv_7__2_input_transpose), 1, 56, 56, 8);
  int8_t* bnn_Conv_7__2_output_bef_transpose = (int8_t *)VTABufferAlloc(200704);
  xp_convolution_wo_tr(bnn_Conv_7__2_input_transpose, A257__2, (int32_t *)conv_bias__33, (int32_t *)Conv_7__2_scalingfactor, bnn_Conv_7__2_output_bef_transpose, 1, 56, 56, 16, 64, 3, 3, 1, 1, 0, 1, 6, 56, 56, vtaCmdH, 2, 8, 56, 0, 1);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(bnn_Conv_7__2_output_bef_transpose), bnn_Conv_7__2_res, 1, 56, 56, 64 );
  VTABufferFree(bnn_Conv_7__2_input_transpose);
  VTABufferFree(bnn_Conv_7__2_output_bef_transpose);

  //Run deallocactivation : dealloc_Sign_6__1_res
  free(Sign_6__1_res);

  //Run elementadd : Add_9__1
  elemadd(bnn_Conv_7__2_res, 1.0/64.000000, 0, Add_5__1_res, 1.0/4.000000, 0, Add_5__1_res, 1.0/4.000000, 0, 200704 );

  //Run deallocactivation : dealloc_bnn_Conv_7__2_res
  free(bnn_Conv_7__2_res);

  //Run allocactivation : Sign_10__1_res
  int8_t *Sign_10__1_res = (int8_t *)malloc(200704);

  //Run elementsign : Sign_10__1
  elemsign(Add_5__1_res, Sign_10__1_res, 200704);
 
  //Run allocactivation : bnn_Conv_11__2_res
  int8_t *bnn_Conv_11__2_res = (int8_t *)malloc(200704);

  //Run bnnconvolution : bnn_Conv_11__2
  int8_t* bnn_Conv_11__2_input_transpose = (int8_t *)VTABufferAlloc(50176);
  transpose_nhwc2vtaio_pack_zerofill(Sign_10__1_res, (int8_t* )VTABufferGetVirtAddr(bnn_Conv_11__2_input_transpose), 1, 56, 56, 8);
  int8_t* bnn_Conv_11__2_output_bef_transpose = (int8_t *)VTABufferAlloc(200704);
  xp_convolution_wo_tr(bnn_Conv_11__2_input_transpose, A258__2, (int32_t *)conv_bias__34, (int32_t *)Conv_11__2_scalingfactor, bnn_Conv_11__2_output_bef_transpose, 1, 56, 56, 16, 64, 3, 3, 1, 1, 0, 1, 7, 56, 56, vtaCmdH, 2, 8, 56, 0, 1);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(bnn_Conv_11__2_output_bef_transpose), bnn_Conv_11__2_res, 1, 56, 56, 64 );
  VTABufferFree(bnn_Conv_11__2_input_transpose);
  VTABufferFree(bnn_Conv_11__2_output_bef_transpose);

  //Run deallocactivation : dealloc_Sign_10__1_res
  free(Sign_10__1_res);

  //Run allocactivation : Add_13__1_res
  int8_t *Add_13__1_res = (int8_t *)malloc(200704);

  //Run elementadd : Add_13__1
  elemadd(bnn_Conv_11__2_res, 1.0/32.000000, 0, Add_5__1_res, 1.0/4.000000, 0, Add_13__1_res, 1.0/2.000000, 0, 200704 );

  //Run deallocactivation : dealloc_Add_5__1_res
  free(Add_5__1_res);

  //Run deallocactivation : dealloc_bnn_Conv_11__2_res
  free(bnn_Conv_11__2_res);

  //Run allocactivation : Sign_14__1_res
  int8_t *Sign_14__1_res = (int8_t *)malloc(200704);

  //Run elementsign : Sign_14__1
  elemsign(Add_13__1_res, Sign_14__1_res, 200704);
 
  //Run allocactivation : bnn_Conv_15__2_res
  int8_t *bnn_Conv_15__2_res = (int8_t *)malloc(200704);

  //Run bnnconvolution : bnn_Conv_15__2
  int8_t* bnn_Conv_15__2_input_transpose = (int8_t *)VTABufferAlloc(50176);
  transpose_nhwc2vtaio_pack_zerofill(Sign_14__1_res, (int8_t* )VTABufferGetVirtAddr(bnn_Conv_15__2_input_transpose), 1, 56, 56, 8);
  int8_t* bnn_Conv_15__2_output_bef_transpose = (int8_t *)VTABufferAlloc(200704);
  xp_convolution_wo_tr(bnn_Conv_15__2_input_transpose, A259__2, (int32_t *)conv_bias__16, (int32_t *)Conv_15__2_scalingfactor, bnn_Conv_15__2_output_bef_transpose, 1, 56, 56, 16, 64, 3, 3, 1, 1, 0, 1, 6, 56, 56, vtaCmdH, 2, 8, 56, 0, 1);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(bnn_Conv_15__2_output_bef_transpose), bnn_Conv_15__2_res, 1, 56, 56, 64 );
  VTABufferFree(bnn_Conv_15__2_input_transpose);
  VTABufferFree(bnn_Conv_15__2_output_bef_transpose);

  //Run deallocactivation : dealloc_Sign_14__1_res
  free(Sign_14__1_res);

  //Run allocactivation : Add_17__1_res
  int8_t *Add_17__1_res = (int8_t *)malloc(200704);

  //Run elementadd : Add_17__1
  elemadd(bnn_Conv_15__2_res, 1.0/32.000000, 0, Add_13__1_res, 1.0/2.000000, 0, Add_17__1_res, 1.0/4.000000, 0, 200704 );

  //Run deallocactivation : dealloc_Add_13__1_res
  free(Add_13__1_res);

  //Run deallocactivation : dealloc_bnn_Conv_15__2_res
  free(bnn_Conv_15__2_res);

  //Run allocactivation : Sign_18__1_res
  int8_t *Sign_18__1_res = (int8_t *)malloc(200704);

  //Run elementsign : Sign_18__1
  elemsign(Add_17__1_res, Sign_18__1_res, 200704);
 
  //Run allocactivation : bnn_Conv_19__2_res
  int8_t *bnn_Conv_19__2_res = (int8_t *)malloc(100352);

  //Run bnnconvolution : bnn_Conv_19__2
  int8_t* bnn_Conv_19__2_input_transpose = (int8_t *)VTABufferAlloc(50176);
  transpose_nhwc2vtaio_pack_zerofill(Sign_18__1_res, (int8_t* )VTABufferGetVirtAddr(bnn_Conv_19__2_input_transpose), 1, 56, 56, 8);
  int8_t* bnn_Conv_19__2_output_bef_transpose = (int8_t *)VTABufferAlloc(100352);
  xp_convolution_wo_tr(bnn_Conv_19__2_input_transpose, A260__2, (int32_t *)conv_bias__35, (int32_t *)Conv_19__2_scalingfactor, bnn_Conv_19__2_output_bef_transpose, 1, 56, 56, 16, 128, 3, 3, 1, 2, 0, 1, 7, 28, 28, vtaCmdH, 2, 8, 28, 0, 1);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(bnn_Conv_19__2_output_bef_transpose), bnn_Conv_19__2_res, 1, 28, 28, 128 );
  VTABufferFree(bnn_Conv_19__2_input_transpose);
  VTABufferFree(bnn_Conv_19__2_output_bef_transpose);

  //Run deallocactivation : dealloc_Sign_18__1_res
  free(Sign_18__1_res);

  //Run allocactivation : AveragePool_22__1_res
  int8_t *AveragePool_22__1_res = (int8_t *)malloc(50176);

  //Run avgpool : AveragePool_22__1
  avgpool(Add_17__1_res, 1.0/4.000000, 0, AveragePool_22__1_res, 1.0/4.000000, 0, 1, 56, 56, 64, 1, 28, 28, 64, 2, 2, 0, 2 );

  //Run deallocactivation : dealloc_Add_17__1_res
  free(Add_17__1_res);

  //Run allocactivation : Add_32__1_res
  int8_t *Add_32__1_res = (int8_t *)malloc(100352);

  //Run convolution : Conv_23__2
  int8_t* Conv_23__2_input_transpose = (int8_t *)VTABufferAlloc(50176);
  transpose_nhwc2vtaio(AveragePool_22__1_res, (int8_t* )VTABufferGetVirtAddr(Conv_23__2_input_transpose), 1, 28, 28, 64);
  int8_t* Conv_23__2_output_bef_transpose = (int8_t *)VTABufferAlloc(100352);
  convolution_wo_tr(Conv_23__2_input_transpose, A248__1, (int32_t *)A249, Conv_23__2_output_bef_transpose, 1, 28, 28, 64, 128, 1, 1, 0, 1, 0, 1, 6, 28, 28, vtaCmdH, 1, 14, 14, 0);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(Conv_23__2_output_bef_transpose), Add_32__1_res, 1, 28, 28, 128 );
  VTABufferFree(Conv_23__2_input_transpose);
  VTABufferFree(Conv_23__2_output_bef_transpose);

  //Run deallocactivation : dealloc_AveragePool_22__1_res
  free(AveragePool_22__1_res);

  //Run elementadd : Add_24__1
  elemadd(bnn_Conv_19__2_res, 1.0/64.000000, 0, Add_32__1_res, 1.0/8.000000, 0, Add_32__1_res, 1.0/8.000000, 0, 100352 );

  //Run deallocactivation : dealloc_bnn_Conv_19__2_res
  free(bnn_Conv_19__2_res);

  //Run allocactivation : Sign_25__1_res
  int8_t *Sign_25__1_res = (int8_t *)malloc(100352);

  //Run elementsign : Sign_25__1
  elemsign(Add_32__1_res, Sign_25__1_res, 100352);
 
  //Run allocactivation : bnn_Conv_26__2_res
  int8_t *bnn_Conv_26__2_res = (int8_t *)malloc(100352);

  //Run bnnconvolution : bnn_Conv_26__2
  int8_t* bnn_Conv_26__2_input_transpose = (int8_t *)VTABufferAlloc(12544);
  transpose_nhwc2vtaio_pack(Sign_25__1_res, (int8_t* )VTABufferGetVirtAddr(bnn_Conv_26__2_input_transpose), 1, 28, 28, 16);
  int8_t* bnn_Conv_26__2_output_bef_transpose = (int8_t *)VTABufferAlloc(100352);
  xp_convolution_wo_tr(bnn_Conv_26__2_input_transpose, A261__2, (int32_t *)conv_bias__36, (int32_t *)Conv_26__2_scalingfactor, bnn_Conv_26__2_output_bef_transpose, 1, 28, 28, 16, 128, 3, 3, 1, 1, 0, 1, 7, 28, 28, vtaCmdH, 2, 14, 28, 0, 1);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(bnn_Conv_26__2_output_bef_transpose), bnn_Conv_26__2_res, 1, 28, 28, 128 );
  VTABufferFree(bnn_Conv_26__2_input_transpose);
  VTABufferFree(bnn_Conv_26__2_output_bef_transpose);

  //Run deallocactivation : dealloc_Sign_25__1_res
  free(Sign_25__1_res);

  //Run elementadd : Add_28__1
  elemadd(bnn_Conv_26__2_res, 1.0/64.000000, 0, Add_32__1_res, 1.0/8.000000, 0, Add_32__1_res, 1.0/8.000000, 0, 100352 );

  //Run deallocactivation : dealloc_bnn_Conv_26__2_res
  free(bnn_Conv_26__2_res);

  //Run allocactivation : Sign_29__1_res
  int8_t *Sign_29__1_res = (int8_t *)malloc(100352);

  //Run elementsign : Sign_29__1
  elemsign(Add_32__1_res, Sign_29__1_res, 100352);
 
  //Run allocactivation : bnn_Conv_30__2_res
  int8_t *bnn_Conv_30__2_res = (int8_t *)malloc(100352);

  //Run bnnconvolution : bnn_Conv_30__2
  int8_t* bnn_Conv_30__2_input_transpose = (int8_t *)VTABufferAlloc(12544);
  transpose_nhwc2vtaio_pack(Sign_29__1_res, (int8_t* )VTABufferGetVirtAddr(bnn_Conv_30__2_input_transpose), 1, 28, 28, 16);
  int8_t* bnn_Conv_30__2_output_bef_transpose = (int8_t *)VTABufferAlloc(100352);
  xp_convolution_wo_tr(bnn_Conv_30__2_input_transpose, A262__2, (int32_t *)conv_bias__37, (int32_t *)Conv_30__2_scalingfactor, bnn_Conv_30__2_output_bef_transpose, 1, 28, 28, 16, 128, 3, 3, 1, 1, 0, 1, 7, 28, 28, vtaCmdH, 2, 14, 28, 0, 1);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(bnn_Conv_30__2_output_bef_transpose), bnn_Conv_30__2_res, 1, 28, 28, 128 );
  VTABufferFree(bnn_Conv_30__2_input_transpose);
  VTABufferFree(bnn_Conv_30__2_output_bef_transpose);

  //Run deallocactivation : dealloc_Sign_29__1_res
  free(Sign_29__1_res);

  //Run elementadd : Add_32__1
  elemadd(bnn_Conv_30__2_res, 1.0/32.000000, 0, Add_32__1_res, 1.0/8.000000, 0, Add_32__1_res, 1.0/8.000000, 0, 100352 );

  //Run deallocactivation : dealloc_bnn_Conv_30__2_res
  free(bnn_Conv_30__2_res);

  //Run allocactivation : Sign_33__1_res
  int8_t *Sign_33__1_res = (int8_t *)malloc(100352);

  //Run elementsign : Sign_33__1
  elemsign(Add_32__1_res, Sign_33__1_res, 100352);
 
  //Run allocactivation : bnn_Conv_34__2_res
  int8_t *bnn_Conv_34__2_res = (int8_t *)malloc(100352);

  //Run bnnconvolution : bnn_Conv_34__2
  int8_t* bnn_Conv_34__2_input_transpose = (int8_t *)VTABufferAlloc(12544);
  transpose_nhwc2vtaio_pack(Sign_33__1_res, (int8_t* )VTABufferGetVirtAddr(bnn_Conv_34__2_input_transpose), 1, 28, 28, 16);
  int8_t* bnn_Conv_34__2_output_bef_transpose = (int8_t *)VTABufferAlloc(100352);
  xp_convolution_wo_tr(bnn_Conv_34__2_input_transpose, A263__2, (int32_t *)conv_bias__20, (int32_t *)Conv_34__2_scalingfactor, bnn_Conv_34__2_output_bef_transpose, 1, 28, 28, 16, 128, 3, 3, 1, 1, 0, 1, 6, 28, 28, vtaCmdH, 2, 14, 28, 0, 1);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(bnn_Conv_34__2_output_bef_transpose), bnn_Conv_34__2_res, 1, 28, 28, 128 );
  VTABufferFree(bnn_Conv_34__2_input_transpose);
  VTABufferFree(bnn_Conv_34__2_output_bef_transpose);

  //Run deallocactivation : dealloc_Sign_33__1_res
  free(Sign_33__1_res);

  //Run elementadd : Add_36__1
  elemadd(bnn_Conv_34__2_res, 1.0/32.000000, 0, Add_32__1_res, 1.0/8.000000, 0, Add_32__1_res, 1.0/8.000000, 0, 100352 );

  //Run deallocactivation : dealloc_bnn_Conv_34__2_res
  free(bnn_Conv_34__2_res);

  //Run allocactivation : Sign_37__1_res
  int8_t *Sign_37__1_res = (int8_t *)malloc(100352);

  //Run elementsign : Sign_37__1
  elemsign(Add_32__1_res, Sign_37__1_res, 100352);
 
  //Run allocactivation : bnn_Conv_38__2_res
  int8_t *bnn_Conv_38__2_res = (int8_t *)malloc(50176);

  //Run bnnconvolution : bnn_Conv_38__2
  int8_t* bnn_Conv_38__2_input_transpose = (int8_t *)VTABufferAlloc(12544);
  transpose_nhwc2vtaio_pack(Sign_37__1_res, (int8_t* )VTABufferGetVirtAddr(bnn_Conv_38__2_input_transpose), 1, 28, 28, 16);
  int8_t* bnn_Conv_38__2_output_bef_transpose = (int8_t *)VTABufferAlloc(50176);
  xp_convolution_wo_tr(bnn_Conv_38__2_input_transpose, A264__2, (int32_t *)conv_bias__38, (int32_t *)Conv_38__2_scalingfactor, bnn_Conv_38__2_output_bef_transpose, 1, 28, 28, 16, 256, 3, 3, 1, 2, 0, 1, 8, 14, 14, vtaCmdH, 2, 14, 14, 0, 1);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(bnn_Conv_38__2_output_bef_transpose), bnn_Conv_38__2_res, 1, 14, 14, 256 );
  VTABufferFree(bnn_Conv_38__2_input_transpose);
  VTABufferFree(bnn_Conv_38__2_output_bef_transpose);

  //Run deallocactivation : dealloc_Sign_37__1_res
  free(Sign_37__1_res);

  //Run allocactivation : AveragePool_41__1_res
  int8_t *AveragePool_41__1_res = (int8_t *)malloc(25088);

  //Run avgpool : AveragePool_41__1
  avgpool(Add_32__1_res, 1.0/8.000000, 0, AveragePool_41__1_res, 1.0/16.000000, 0, 1, 28, 28, 128, 1, 14, 14, 128, 2, 2, 0, 2 );

  //Run deallocactivation : dealloc_Add_32__1_res
  free(Add_32__1_res);

  //Run allocactivation : Add_43__1_res
  int8_t *Add_43__1_res = (int8_t *)malloc(50176);

  //Run convolution : Conv_42__2
  int8_t* Conv_42__2_input_transpose = (int8_t *)VTABufferAlloc(25088);
  transpose_nhwc2vtaio(AveragePool_41__1_res, (int8_t* )VTABufferGetVirtAddr(Conv_42__2_input_transpose), 1, 14, 14, 128);
  int8_t* Conv_42__2_output_bef_transpose = (int8_t *)VTABufferAlloc(50176);
  convolution_wo_tr(Conv_42__2_input_transpose, A251__1, (int32_t *)A252, Conv_42__2_output_bef_transpose, 1, 14, 14, 128, 256, 1, 1, 0, 1, 0, 1, 8, 14, 14, vtaCmdH, 1, 14, 14, 0);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(Conv_42__2_output_bef_transpose), Add_43__1_res, 1, 14, 14, 256 );
  VTABufferFree(Conv_42__2_input_transpose);
  VTABufferFree(Conv_42__2_output_bef_transpose);

  //Run deallocactivation : dealloc_AveragePool_41__1_res
  free(AveragePool_41__1_res);

  //Run elementadd : Add_43__1
  elemadd(bnn_Conv_38__2_res, 1.0/32.000000, 0, Add_43__1_res, 1.0/16.000000, 0, Add_43__1_res, 1.0/16.000000, 0, 50176 );

  //Run deallocactivation : dealloc_bnn_Conv_38__2_res
  free(bnn_Conv_38__2_res);

  //Run allocactivation : Sign_44__1_res
  int8_t *Sign_44__1_res = (int8_t *)malloc(50176);

  //Run elementsign : Sign_44__1
  elemsign(Add_43__1_res, Sign_44__1_res, 50176);
 
  //Run allocactivation : bnn_Conv_45__2_res
  int8_t *bnn_Conv_45__2_res = (int8_t *)malloc(50176);

  //Run bnnconvolution : bnn_Conv_45__2
  int8_t* bnn_Conv_45__2_input_transpose = (int8_t *)VTABufferAlloc(6272);
  transpose_nhwc2vtaio_pack(Sign_44__1_res, (int8_t* )VTABufferGetVirtAddr(bnn_Conv_45__2_input_transpose), 1, 14, 14, 32);
  int8_t* bnn_Conv_45__2_output_bef_transpose = (int8_t *)VTABufferAlloc(50176);
  xp_convolution_wo_tr(bnn_Conv_45__2_input_transpose, A265__2, (int32_t *)conv_bias__39, (int32_t *)Conv_45__2_scalingfactor, bnn_Conv_45__2_output_bef_transpose, 1, 14, 14, 32, 256, 3, 3, 1, 1, 0, 1, 8, 14, 14, vtaCmdH, 2, 14, 14, 0, 1);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(bnn_Conv_45__2_output_bef_transpose), bnn_Conv_45__2_res, 1, 14, 14, 256 );
  VTABufferFree(bnn_Conv_45__2_input_transpose);
  VTABufferFree(bnn_Conv_45__2_output_bef_transpose);

  //Run deallocactivation : dealloc_Sign_44__1_res
  free(Sign_44__1_res);

  //Run elementadd : Add_47__1
  elemadd(bnn_Conv_45__2_res, 1.0/32.000000, 0, Add_43__1_res, 1.0/16.000000, 0, Add_43__1_res, 1.0/16.000000, 0, 50176 );

  //Run deallocactivation : dealloc_bnn_Conv_45__2_res
  free(bnn_Conv_45__2_res);

  //Run allocactivation : Sign_48__1_res
  int8_t *Sign_48__1_res = (int8_t *)malloc(50176);

  //Run elementsign : Sign_48__1
  elemsign(Add_43__1_res, Sign_48__1_res, 50176);
 
  //Run allocactivation : bnn_Conv_49__2_res
  int8_t *bnn_Conv_49__2_res = (int8_t *)malloc(50176);

  //Run bnnconvolution : bnn_Conv_49__2
  int8_t* bnn_Conv_49__2_input_transpose = (int8_t *)VTABufferAlloc(6272);
  transpose_nhwc2vtaio_pack(Sign_48__1_res, (int8_t* )VTABufferGetVirtAddr(bnn_Conv_49__2_input_transpose), 1, 14, 14, 32);
  int8_t* bnn_Conv_49__2_output_bef_transpose = (int8_t *)VTABufferAlloc(50176);
  xp_convolution_wo_tr(bnn_Conv_49__2_input_transpose, A266__2, (int32_t *)conv_bias__40, (int32_t *)Conv_49__2_scalingfactor, bnn_Conv_49__2_output_bef_transpose, 1, 14, 14, 32, 256, 3, 3, 1, 1, 0, 1, 7, 14, 14, vtaCmdH, 2, 14, 14, 0, 1);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(bnn_Conv_49__2_output_bef_transpose), bnn_Conv_49__2_res, 1, 14, 14, 256 );
  VTABufferFree(bnn_Conv_49__2_input_transpose);
  VTABufferFree(bnn_Conv_49__2_output_bef_transpose);

  //Run deallocactivation : dealloc_Sign_48__1_res
  free(Sign_48__1_res);

  //Run allocactivation : Add_51__1_res
  int8_t *Add_51__1_res = (int8_t *)malloc(50176);

  //Run elementadd : Add_51__1
  elemadd(bnn_Conv_49__2_res, 1.0/32.000000, 0, Add_43__1_res, 1.0/16.000000, 0, Add_51__1_res, 1.0/8.000000, 0, 50176 );

  //Run deallocactivation : dealloc_Add_43__1_res
  free(Add_43__1_res);

  //Run deallocactivation : dealloc_bnn_Conv_49__2_res
  free(bnn_Conv_49__2_res);

  //Run allocactivation : Sign_52__1_res
  int8_t *Sign_52__1_res = (int8_t *)malloc(50176);

  //Run elementsign : Sign_52__1
  elemsign(Add_51__1_res, Sign_52__1_res, 50176);
 
  //Run allocactivation : bnn_Conv_53__2_res
  int8_t *bnn_Conv_53__2_res = (int8_t *)malloc(50176);

  //Run bnnconvolution : bnn_Conv_53__2
  int8_t* bnn_Conv_53__2_input_transpose = (int8_t *)VTABufferAlloc(6272);
  transpose_nhwc2vtaio_pack(Sign_52__1_res, (int8_t* )VTABufferGetVirtAddr(bnn_Conv_53__2_input_transpose), 1, 14, 14, 32);
  int8_t* bnn_Conv_53__2_output_bef_transpose = (int8_t *)VTABufferAlloc(50176);
  xp_convolution_wo_tr(bnn_Conv_53__2_input_transpose, A267__2, (int32_t *)conv_bias__24, (int32_t *)Conv_53__2_scalingfactor, bnn_Conv_53__2_output_bef_transpose, 1, 14, 14, 32, 256, 3, 3, 1, 1, 0, 1, 7, 14, 14, vtaCmdH, 2, 14, 14, 0, 1);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(bnn_Conv_53__2_output_bef_transpose), bnn_Conv_53__2_res, 1, 14, 14, 256 );
  VTABufferFree(bnn_Conv_53__2_input_transpose);
  VTABufferFree(bnn_Conv_53__2_output_bef_transpose);

  //Run deallocactivation : dealloc_Sign_52__1_res
  free(Sign_52__1_res);

  //Run elementadd : Add_55__1
  elemadd(bnn_Conv_53__2_res, 1.0/16.000000, 0, Add_51__1_res, 1.0/8.000000, 0, Add_51__1_res, 1.0/8.000000, 0, 50176 );

  //Run deallocactivation : dealloc_bnn_Conv_53__2_res
  free(bnn_Conv_53__2_res);

  //Run allocactivation : Sign_56__1_res
  int8_t *Sign_56__1_res = (int8_t *)malloc(50176);

  //Run elementsign : Sign_56__1
  elemsign(Add_51__1_res, Sign_56__1_res, 50176);
 
  //Run allocactivation : bnn_Conv_57__2_res
  int8_t *bnn_Conv_57__2_res = (int8_t *)malloc(25088);

  //Run bnnconvolution : bnn_Conv_57__2
  int8_t* bnn_Conv_57__2_input_transpose = (int8_t *)VTABufferAlloc(6272);
  transpose_nhwc2vtaio_pack(Sign_56__1_res, (int8_t* )VTABufferGetVirtAddr(bnn_Conv_57__2_input_transpose), 1, 14, 14, 32);
  int8_t* bnn_Conv_57__2_output_bef_transpose = (int8_t *)VTABufferAlloc(25088);
  xp_convolution_wo_tr(bnn_Conv_57__2_input_transpose, A268__2, (int32_t *)conv_bias__41, (int32_t *)Conv_57__2_scalingfactor, bnn_Conv_57__2_output_bef_transpose, 1, 14, 14, 32, 512, 3, 3, 1, 2, 0, 1, 8, 7, 7, vtaCmdH, 2, 7, 7, 0, 1);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(bnn_Conv_57__2_output_bef_transpose), bnn_Conv_57__2_res, 1, 7, 7, 512 );
  VTABufferFree(bnn_Conv_57__2_input_transpose);
  VTABufferFree(bnn_Conv_57__2_output_bef_transpose);

  //Run deallocactivation : dealloc_Sign_56__1_res
  free(Sign_56__1_res);

  //Run allocactivation : AveragePool_60__1_res
  int8_t *AveragePool_60__1_res = (int8_t *)malloc(12544);

  //Run avgpool : AveragePool_60__1
  avgpool(Add_51__1_res, 1.0/8.000000, 0, AveragePool_60__1_res, 1.0/8.000000, 0, 1, 14, 14, 256, 1, 7, 7, 256, 2, 2, 0, 2 );

  //Run deallocactivation : dealloc_Add_51__1_res
  free(Add_51__1_res);

  //Run allocactivation : Add_70__1_res
  int8_t *Add_70__1_res = (int8_t *)malloc(25088);

  //Run convolution : Conv_61__2
  int8_t* Conv_61__2_input_transpose = (int8_t *)VTABufferAlloc(12544);
  transpose_nhwc2vtaio(AveragePool_60__1_res, (int8_t* )VTABufferGetVirtAddr(Conv_61__2_input_transpose), 1, 7, 7, 256);
  int8_t* Conv_61__2_output_bef_transpose = (int8_t *)VTABufferAlloc(25088);
  convolution_wo_tr(Conv_61__2_input_transpose, A254__1, (int32_t *)A255, Conv_61__2_output_bef_transpose, 1, 7, 7, 256, 512, 1, 1, 0, 1, 0, 1, 7, 7, 7, vtaCmdH, 1, 14, 14, 0);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(Conv_61__2_output_bef_transpose), Add_70__1_res, 1, 7, 7, 512 );
  VTABufferFree(Conv_61__2_input_transpose);
  VTABufferFree(Conv_61__2_output_bef_transpose);

  //Run deallocactivation : dealloc_AveragePool_60__1_res
  free(AveragePool_60__1_res);

  //Run elementadd : Add_62__1
  elemadd(bnn_Conv_57__2_res, 1.0/32.000000, 0, Add_70__1_res, 1.0/16.000000, 0, Add_70__1_res, 1.0/16.000000, 0, 25088 );

  //Run deallocactivation : dealloc_bnn_Conv_57__2_res
  free(bnn_Conv_57__2_res);

  //Run allocactivation : Sign_63__1_res
  int8_t *Sign_63__1_res = (int8_t *)malloc(25088);

  //Run elementsign : Sign_63__1
  elemsign(Add_70__1_res, Sign_63__1_res, 25088);
 
  //Run allocactivation : bnn_Conv_64__2_res
  int8_t *bnn_Conv_64__2_res = (int8_t *)malloc(25088);

  //Run bnnconvolution : bnn_Conv_64__2
  int8_t* bnn_Conv_64__2_input_transpose = (int8_t *)VTABufferAlloc(3136);
  transpose_nhwc2vtaio_pack(Sign_63__1_res, (int8_t* )VTABufferGetVirtAddr(bnn_Conv_64__2_input_transpose), 1, 7, 7, 64);
  int8_t* bnn_Conv_64__2_output_bef_transpose = (int8_t *)VTABufferAlloc(25088);
  xp_convolution_wo_tr(bnn_Conv_64__2_input_transpose, A269__2, (int32_t *)conv_bias__42, (int32_t *)Conv_64__2_scalingfactor, bnn_Conv_64__2_output_bef_transpose, 1, 7, 7, 64, 512, 3, 3, 1, 1, 0, 1, 8, 7, 7, vtaCmdH, 2, 7, 7, 0, 1);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(bnn_Conv_64__2_output_bef_transpose), bnn_Conv_64__2_res, 1, 7, 7, 512 );
  VTABufferFree(bnn_Conv_64__2_input_transpose);
  VTABufferFree(bnn_Conv_64__2_output_bef_transpose);

  //Run deallocactivation : dealloc_Sign_63__1_res
  free(Sign_63__1_res);

  //Run elementadd : Add_66__1
  elemadd(bnn_Conv_64__2_res, 1.0/32.000000, 0, Add_70__1_res, 1.0/16.000000, 0, Add_70__1_res, 1.0/16.000000, 0, 25088 );

  //Run deallocactivation : dealloc_bnn_Conv_64__2_res
  free(bnn_Conv_64__2_res);

  //Run allocactivation : Sign_67__1_res
  int8_t *Sign_67__1_res = (int8_t *)malloc(25088);

  //Run elementsign : Sign_67__1
  elemsign(Add_70__1_res, Sign_67__1_res, 25088);
 
  //Run allocactivation : bnn_Conv_68__2_res
  int8_t *bnn_Conv_68__2_res = (int8_t *)malloc(25088);

  //Run bnnconvolution : bnn_Conv_68__2
  int8_t* bnn_Conv_68__2_input_transpose = (int8_t *)VTABufferAlloc(3136);
  transpose_nhwc2vtaio_pack(Sign_67__1_res, (int8_t* )VTABufferGetVirtAddr(bnn_Conv_68__2_input_transpose), 1, 7, 7, 64);
  int8_t* bnn_Conv_68__2_output_bef_transpose = (int8_t *)VTABufferAlloc(25088);
  xp_convolution_wo_tr(bnn_Conv_68__2_input_transpose, A270__2, (int32_t *)conv_bias__43, (int32_t *)Conv_68__2_scalingfactor, bnn_Conv_68__2_output_bef_transpose, 1, 7, 7, 64, 512, 3, 3, 1, 1, 0, 1, 8, 7, 7, vtaCmdH, 2, 7, 7, 0, 1);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(bnn_Conv_68__2_output_bef_transpose), bnn_Conv_68__2_res, 1, 7, 7, 512 );
  VTABufferFree(bnn_Conv_68__2_input_transpose);
  VTABufferFree(bnn_Conv_68__2_output_bef_transpose);

  //Run deallocactivation : dealloc_Sign_67__1_res
  free(Sign_67__1_res);

  //Run elementadd : Add_70__1
  elemadd(bnn_Conv_68__2_res, 1.0/32.000000, 0, Add_70__1_res, 1.0/16.000000, 0, Add_70__1_res, 1.0/16.000000, 0, 25088 );

  //Run deallocactivation : dealloc_bnn_Conv_68__2_res
  free(bnn_Conv_68__2_res);

  //Run allocactivation : Sign_71__1_res
  int8_t *Sign_71__1_res = (int8_t *)malloc(25088);

  //Run elementsign : Sign_71__1
  elemsign(Add_70__1_res, Sign_71__1_res, 25088);
 
  //Run allocactivation : bnn_Conv_72__2_res
  int8_t *bnn_Conv_72__2_res = (int8_t *)malloc(25088);

  //Run bnnconvolution : bnn_Conv_72__2
  int8_t* bnn_Conv_72__2_input_transpose = (int8_t *)VTABufferAlloc(3136);
  transpose_nhwc2vtaio_pack(Sign_71__1_res, (int8_t* )VTABufferGetVirtAddr(bnn_Conv_72__2_input_transpose), 1, 7, 7, 64);
  int8_t* bnn_Conv_72__2_output_bef_transpose = (int8_t *)VTABufferAlloc(25088);
  xp_convolution_wo_tr(bnn_Conv_72__2_input_transpose, A271__2, (int32_t *)conv_bias__28, (int32_t *)Conv_72__2_scalingfactor, bnn_Conv_72__2_output_bef_transpose, 1, 7, 7, 64, 512, 3, 3, 1, 1, 0, 1, 8, 7, 7, vtaCmdH, 2, 7, 7, 0, 1);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(bnn_Conv_72__2_output_bef_transpose), bnn_Conv_72__2_res, 1, 7, 7, 512 );
  VTABufferFree(bnn_Conv_72__2_input_transpose);
  VTABufferFree(bnn_Conv_72__2_output_bef_transpose);

  //Run deallocactivation : dealloc_Sign_71__1_res
  free(Sign_71__1_res);

  //Run elementadd : Add_74__1
  elemadd(bnn_Conv_72__2_res, 1.0/32.000000, 0, Add_70__1_res, 1.0/16.000000, 0, Add_70__1_res, 1.0/16.000000, 0, 25088 );

  //Run deallocactivation : dealloc_bnn_Conv_72__2_res
  free(bnn_Conv_72__2_res);

  //Run allocactivation : GlobalAveragePool_75__1_res
  int8_t *GlobalAveragePool_75__1_res = (int8_t *)malloc(512);

  //Run avgpool : GlobalAveragePool_75__1
  avgpool(Add_70__1_res, 1.0/16.000000, 0, GlobalAveragePool_75__1_res, 1.0/16.000000, 0, 1, 7, 7, 512, 1, 1, 1, 512, 7, 7, 0, 1 );

  //Run deallocactivation : dealloc_Add_70__1_res
  free(Add_70__1_res);

  //Run tensorview : GlobalAveragePool_75__1_res__2
  int8_t* GlobalAveragePool_75__1_res__2 = GlobalAveragePool_75__1_res;

  //Run allocactivation : Gemm_82__1_res
  int8_t *Gemm_82__1_res = (int8_t *)malloc(1000);

  //Run fullyconnected : Gemm_82__1
  fullyconnected(GlobalAveragePool_75__1_res__2, 1.0/16.000000, 0, (int8_t *)VTABufferGetVirtAddr(fc_weight__1), 1.0/64.000000, 0, (int8_t *)VTABufferGetVirtAddr(fc_bias), 1.0/1024.000000, 0, Gemm_82__1_res, 1.0/2.000000, 0, 1, 512, 512, 1000, 1, 1000, 1 );

  //Run deallocactivation : dealloc_GlobalAveragePool_75__1_res
  free(GlobalAveragePool_75__1_res);

  //Run dequantize : Gemm_82__1_dequantize
  int8_t* output = (int8_t*)mutableWeight + 602112;
  dequantize(Gemm_82__1_res, output, 1000, 1/2.000000, 0 );

  //Run deallocactivation : dealloc_Gemm_82__1_res
  free(Gemm_82__1_res);
  return 0;
}