

#include "vta/runtime.h"
#include "VTABundle.h"
#include <time.h>
#include <iostream>
#include <fstream>
#include "vtaFloatConvTestMainEntry.h"
SymbolTableEntry symbolTableEntry_vtaFloatConvTestBundle[2]={{"inputP",0,150528,'1'},{"outP",602112,802816,'1'}};
BundleConfig vtaFloatConvTestBundle_config = {37888, 3813376, 0, 64, 2, symbolTableEntry_vtaFloatConvTestBundle};
int8_t* filterP;
int8_t* biasP;
extern VTACommandHandle vtaCmdH;

void vtaFloatConvTestMainEntry_load_module(uint8_t *constantWeight){
  filterP = (int8_t *)VTABufferAlloc(37632);
  VTABufferCopy((int8_t *)(constantWeight + 0), 0, filterP, 0, 37632, 1);
  biasP = (int8_t *)VTABufferAlloc(256);
  VTABufferCopy((int8_t *)(constantWeight + 37632), 0, biasP, 0, 256, 1);
}

void vtaFloatConvTestMainEntry_destroy_module(){
  VTABufferFree(filterP);
  VTABufferFree(biasP);
}
int vtaFloatConvTestMainEntry(uint8_t *constantWeight, uint8_t *mutableWeight, uint8_t *activations){

  //Run convolution : conv
  int8_t* inputP = (int8_t*)mutableWeight + 0;
  int8_t* outP = (int8_t*)mutableWeight + 602112;
  convolutionFloat(inputP, (int8_t*)VTABufferGetVirtAddr(filterP), (int8_t *)VTABufferGetVirtAddr(biasP), outP, 1, 224, 224, 3, 64, 7, 7, 3, 2, 1, 1, 0, 1, 112, 112 );
  return 0;
}