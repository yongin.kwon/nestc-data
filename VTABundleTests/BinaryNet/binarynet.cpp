

#include "VTABundle.h"
#include "vta/runtime.h"
#include <fstream>
#include <iostream>
#include <time.h>
#include "binarynet.h"
SymbolTableEntry symbolTableEntry_binarynet[2]={{"input",0,3072,'1'},{"output",12288,10,'1'}};
BundleConfig binarynet_config = {1402408, 12328, 0, 64, 2, symbolTableEntry_binarynet};
namespace namespace_binarynet {
int8_t* conv1_weight__1;
int8_t* conv1_bias;
int8_t* layer1_0_conv1_weight__2;
int8_t* layer1_0_conv1_bias__1;
int8_t* layer1_0_conv2_weight__2;
int8_t* layer1_0_conv2_bias__1;
int8_t* layer1_1_conv1_weight__2;
int8_t* layer1_1_conv1_bias__1;
int8_t* layer1_1_conv2_weight__2;
int8_t* layer1_1_conv2_bias__1;
int8_t* layer2_0_conv1_weight__2;
int8_t* layer2_0_conv1_bias__1;
int8_t* layer2_0_conv2_weight__2;
int8_t* layer2_0_conv2_bias__1;
int8_t* layer2_1_conv1_weight__2;
int8_t* layer2_1_conv1_bias__1;
int8_t* layer2_1_conv2_weight__2;
int8_t* layer2_1_conv2_bias__1;
int8_t* layer3_0_conv1_weight__2;
int8_t* layer3_0_conv1_bias__1;
int8_t* layer3_0_conv2_weight__2;
int8_t* layer3_0_conv2_bias__1;
int8_t* layer3_1_conv1_weight__2;
int8_t* layer3_1_conv1_bias__1;
int8_t* layer3_1_conv2_weight__2;
int8_t* layer3_1_conv2_bias__1;
int8_t* fc_weight__1;
int8_t* fc_bias;
}
using namespace namespace_binarynet;
extern VTACommandHandle vtaCmdH;

void binarynet_load_module(uint8_t *constantWeight){
  conv1_weight__1 = (int8_t *)VTABufferAlloc(18432);
  VTABufferCopy((int8_t *)(constantWeight + 0), 0, conv1_weight__1, 0, 18432, 1);
  conv1_bias = (int8_t *)VTABufferAlloc(512);
  VTABufferCopy((int8_t *)(constantWeight + 18432), 0, conv1_bias, 0, 512, 1);
  layer1_0_conv1_weight__2 = (int8_t *)VTABufferAlloc(18432);
  VTABufferCopy((int8_t *)(constantWeight + 18944), 0, layer1_0_conv1_weight__2, 0, 18432, 1);
  layer1_0_conv1_bias__1 = (int8_t *)VTABufferAlloc(512);
  VTABufferCopy((int8_t *)(constantWeight + 37376), 0, layer1_0_conv1_bias__1, 0, 512, 1);
  layer1_0_conv2_weight__2 = (int8_t *)VTABufferAlloc(18432);
  VTABufferCopy((int8_t *)(constantWeight + 37888), 0, layer1_0_conv2_weight__2, 0, 18432, 1);
  layer1_0_conv2_bias__1 = (int8_t *)VTABufferAlloc(512);
  VTABufferCopy((int8_t *)(constantWeight + 56320), 0, layer1_0_conv2_bias__1, 0, 512, 1);
  layer1_1_conv1_weight__2 = (int8_t *)VTABufferAlloc(18432);
  VTABufferCopy((int8_t *)(constantWeight + 56832), 0, layer1_1_conv1_weight__2, 0, 18432, 1);
  layer1_1_conv1_bias__1 = (int8_t *)VTABufferAlloc(512);
  VTABufferCopy((int8_t *)(constantWeight + 75264), 0, layer1_1_conv1_bias__1, 0, 512, 1);
  layer1_1_conv2_weight__2 = (int8_t *)VTABufferAlloc(18432);
  VTABufferCopy((int8_t *)(constantWeight + 75776), 0, layer1_1_conv2_weight__2, 0, 18432, 1);
  layer1_1_conv2_bias__1 = (int8_t *)VTABufferAlloc(512);
  VTABufferCopy((int8_t *)(constantWeight + 94208), 0, layer1_1_conv2_bias__1, 0, 512, 1);
  layer2_0_conv1_weight__2 = (int8_t *)VTABufferAlloc(36864);
  VTABufferCopy((int8_t *)(constantWeight + 94720), 0, layer2_0_conv1_weight__2, 0, 36864, 1);
  layer2_0_conv1_bias__1 = (int8_t *)VTABufferAlloc(1024);
  VTABufferCopy((int8_t *)(constantWeight + 131584), 0, layer2_0_conv1_bias__1, 0, 1024, 1);
  layer2_0_conv2_weight__2 = (int8_t *)VTABufferAlloc(73728);
  VTABufferCopy((int8_t *)(constantWeight + 132608), 0, layer2_0_conv2_weight__2, 0, 73728, 1);
  layer2_0_conv2_bias__1 = (int8_t *)VTABufferAlloc(1024);
  VTABufferCopy((int8_t *)(constantWeight + 206336), 0, layer2_0_conv2_bias__1, 0, 1024, 1);
  layer2_1_conv1_weight__2 = (int8_t *)VTABufferAlloc(73728);
  VTABufferCopy((int8_t *)(constantWeight + 207360), 0, layer2_1_conv1_weight__2, 0, 73728, 1);
  layer2_1_conv1_bias__1 = (int8_t *)VTABufferAlloc(1024);
  VTABufferCopy((int8_t *)(constantWeight + 281088), 0, layer2_1_conv1_bias__1, 0, 1024, 1);
  layer2_1_conv2_weight__2 = (int8_t *)VTABufferAlloc(73728);
  VTABufferCopy((int8_t *)(constantWeight + 282112), 0, layer2_1_conv2_weight__2, 0, 73728, 1);
  layer2_1_conv2_bias__1 = (int8_t *)VTABufferAlloc(1024);
  VTABufferCopy((int8_t *)(constantWeight + 355840), 0, layer2_1_conv2_bias__1, 0, 1024, 1);
  layer3_0_conv1_weight__2 = (int8_t *)VTABufferAlloc(147456);
  VTABufferCopy((int8_t *)(constantWeight + 356864), 0, layer3_0_conv1_weight__2, 0, 147456, 1);
  layer3_0_conv1_bias__1 = (int8_t *)VTABufferAlloc(2048);
  VTABufferCopy((int8_t *)(constantWeight + 504320), 0, layer3_0_conv1_bias__1, 0, 2048, 1);
  layer3_0_conv2_weight__2 = (int8_t *)VTABufferAlloc(294912);
  VTABufferCopy((int8_t *)(constantWeight + 506368), 0, layer3_0_conv2_weight__2, 0, 294912, 1);
  layer3_0_conv2_bias__1 = (int8_t *)VTABufferAlloc(2048);
  VTABufferCopy((int8_t *)(constantWeight + 801280), 0, layer3_0_conv2_bias__1, 0, 2048, 1);
  layer3_1_conv1_weight__2 = (int8_t *)VTABufferAlloc(294912);
  VTABufferCopy((int8_t *)(constantWeight + 803328), 0, layer3_1_conv1_weight__2, 0, 294912, 1);
  layer3_1_conv1_bias__1 = (int8_t *)VTABufferAlloc(2048);
  VTABufferCopy((int8_t *)(constantWeight + 1098240), 0, layer3_1_conv1_bias__1, 0, 2048, 1);
  layer3_1_conv2_weight__2 = (int8_t *)VTABufferAlloc(294912);
  VTABufferCopy((int8_t *)(constantWeight + 1100288), 0, layer3_1_conv2_weight__2, 0, 294912, 1);
  layer3_1_conv2_bias__1 = (int8_t *)VTABufferAlloc(2048);
  VTABufferCopy((int8_t *)(constantWeight + 1395200), 0, layer3_1_conv2_bias__1, 0, 2048, 1);
  fc_weight__1 = (int8_t *)VTABufferAlloc(5120);
  VTABufferCopy((int8_t *)(constantWeight + 1397248), 0, fc_weight__1, 0, 5120, 1);
  fc_bias = (int8_t *)VTABufferAlloc(40);
  VTABufferCopy((int8_t *)(constantWeight + 1402368), 0, fc_bias, 0, 40, 1);
}

void binarynet_destroy_module(){
  VTABufferFree(conv1_weight__1);
  VTABufferFree(conv1_bias);
  VTABufferFree(layer1_0_conv1_weight__2);
  VTABufferFree(layer1_0_conv1_bias__1);
  VTABufferFree(layer1_0_conv2_weight__2);
  VTABufferFree(layer1_0_conv2_bias__1);
  VTABufferFree(layer1_1_conv1_weight__2);
  VTABufferFree(layer1_1_conv1_bias__1);
  VTABufferFree(layer1_1_conv2_weight__2);
  VTABufferFree(layer1_1_conv2_bias__1);
  VTABufferFree(layer2_0_conv1_weight__2);
  VTABufferFree(layer2_0_conv1_bias__1);
  VTABufferFree(layer2_0_conv2_weight__2);
  VTABufferFree(layer2_0_conv2_bias__1);
  VTABufferFree(layer2_1_conv1_weight__2);
  VTABufferFree(layer2_1_conv1_bias__1);
  VTABufferFree(layer2_1_conv2_weight__2);
  VTABufferFree(layer2_1_conv2_bias__1);
  VTABufferFree(layer3_0_conv1_weight__2);
  VTABufferFree(layer3_0_conv1_bias__1);
  VTABufferFree(layer3_0_conv2_weight__2);
  VTABufferFree(layer3_0_conv2_bias__1);
  VTABufferFree(layer3_1_conv1_weight__2);
  VTABufferFree(layer3_1_conv1_bias__1);
  VTABufferFree(layer3_1_conv2_weight__2);
  VTABufferFree(layer3_1_conv2_bias__1);
  VTABufferFree(fc_weight__1);
  VTABufferFree(fc_bias);
}
int binarynet(uint8_t *constantWeight, uint8_t *mutableWeight, uint8_t *activations){

  //Run allocactivation : Conv_0__1_quantize_res
  int8_t *Conv_0__1_quantize_res = (int8_t *)malloc(3072);

  //Run quantize : Conv_0__1_quantize
  int8_t* input = (int8_t*)mutableWeight + 0;
  quantize(input, Conv_0__1_quantize_res, 3072, 1/64.000000, 0 );

  //Run allocactivation : Conv_0__8_res
  int8_t *Conv_0__8_res = (int8_t *)malloc(3072);

  //Run transpose : Conv_0__8
  transpose(Conv_0__1_quantize_res, Conv_0__8_res, 1, 3, 32, 32, 1, 32, 32, 3, 0, 2, 3, 1 );

  //Run deallocactivation : dealloc_Conv_0__1_quantize_res
  free(Conv_0__1_quantize_res);

  //Run allocactivation : Conv_0__2_res
  int8_t *Conv_0__2_res = (int8_t *)malloc(131072);

  //Run convolution : Conv_0__2
  int8_t* Conv_0__2_input_transpose = (int8_t *)VTABufferAlloc(16384);
  transpose_nhwc2vtaio_zerofill(Conv_0__8_res, (int8_t* )VTABufferGetVirtAddr(Conv_0__2_input_transpose), 1, 32, 32, 16);
  int8_t* Conv_0__2_output_bef_transpose = (int8_t *)VTABufferAlloc(131072);
  convolution_wo_tr(Conv_0__2_input_transpose, conv1_weight__1, (int32_t *)conv1_bias, Conv_0__2_output_bef_transpose, 1, 32, 32, 16, 128, 3, 3, 1, 1, 0, 1, 10, 32, 32, vtaCmdH, 1, 8, 8, 0);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(Conv_0__2_output_bef_transpose), Conv_0__2_res, 1, 32, 32, 128 );
  VTABufferFree(Conv_0__2_input_transpose);
  VTABufferFree(Conv_0__2_output_bef_transpose);

  //Run deallocactivation : dealloc_Conv_0__8_res
  free(Conv_0__8_res);

  //Run allocactivation : Sign_1__1_res
  int8_t *Sign_1__1_res = (int8_t *)malloc(131072);

  //Run elementsign : Sign_1__1
  elemsign(Conv_0__2_res, Sign_1__1_res, 131072);
 
  //Run deallocactivation : dealloc_Conv_0__2_res
  free(Conv_0__2_res);

  //Run allocactivation : bnn_Conv_2__2_res
  int8_t *bnn_Conv_2__2_res = (int8_t *)malloc(131072);

  //Run bnnconvolution : bnn_Conv_2__2
  int8_t* bnn_Conv_2__2_input_transpose = (int8_t *)VTABufferAlloc(16384);
  transpose_nhwc2vtaio_pack(Sign_1__1_res, (int8_t* )VTABufferGetVirtAddr(bnn_Conv_2__2_input_transpose), 1, 32, 32, 16);
  int8_t* bnn_Conv_2__2_output_bef_transpose = (int8_t *)VTABufferAlloc(131072);
  xp_convolution_wo_tr(bnn_Conv_2__2_input_transpose, layer1_0_conv1_weight__2, (int32_t *)layer1_0_conv1_bias__1, (int32_t *)layer1_0_conv1_bias__1, bnn_Conv_2__2_output_bef_transpose, 1, 32, 32, 16, 128, 3, 3, 1, 1, 0, 1, 8, 32, 32, vtaCmdH, 1, 8, 8, 0, 0);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(bnn_Conv_2__2_output_bef_transpose), bnn_Conv_2__2_res, 1, 32, 32, 128 );
  VTABufferFree(bnn_Conv_2__2_input_transpose);
  VTABufferFree(bnn_Conv_2__2_output_bef_transpose);

  //Run deallocactivation : dealloc_Sign_1__1_res
  free(Sign_1__1_res);

  //Run allocactivation : Sign_3__1_res
  int8_t *Sign_3__1_res = (int8_t *)malloc(131072);

  //Run elementsign : Sign_3__1
  elemsign(bnn_Conv_2__2_res, Sign_3__1_res, 131072);
 
  //Run deallocactivation : dealloc_bnn_Conv_2__2_res
  free(bnn_Conv_2__2_res);

  //Run allocactivation : bnn_Conv_4__2_res
  int8_t *bnn_Conv_4__2_res = (int8_t *)malloc(131072);

  //Run bnnconvolution : bnn_Conv_4__2
  int8_t* bnn_Conv_4__2_input_transpose = (int8_t *)VTABufferAlloc(16384);
  transpose_nhwc2vtaio_pack(Sign_3__1_res, (int8_t* )VTABufferGetVirtAddr(bnn_Conv_4__2_input_transpose), 1, 32, 32, 16);
  int8_t* bnn_Conv_4__2_output_bef_transpose = (int8_t *)VTABufferAlloc(131072);
  xp_convolution_wo_tr(bnn_Conv_4__2_input_transpose, layer1_0_conv2_weight__2, (int32_t *)layer1_0_conv2_bias__1, (int32_t *)layer1_0_conv2_bias__1, bnn_Conv_4__2_output_bef_transpose, 1, 32, 32, 16, 128, 3, 3, 1, 1, 0, 1, 7, 32, 32, vtaCmdH, 1, 8, 8, 0, 0);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(bnn_Conv_4__2_output_bef_transpose), bnn_Conv_4__2_res, 1, 32, 32, 128 );
  VTABufferFree(bnn_Conv_4__2_input_transpose);
  VTABufferFree(bnn_Conv_4__2_output_bef_transpose);

  //Run deallocactivation : dealloc_Sign_3__1_res
  free(Sign_3__1_res);

  //Run allocactivation : Sign_5__1_res
  int8_t *Sign_5__1_res = (int8_t *)malloc(131072);

  //Run elementsign : Sign_5__1
  elemsign(bnn_Conv_4__2_res, Sign_5__1_res, 131072);
 
  //Run deallocactivation : dealloc_bnn_Conv_4__2_res
  free(bnn_Conv_4__2_res);

  //Run allocactivation : bnn_Conv_6__2_res
  int8_t *bnn_Conv_6__2_res = (int8_t *)malloc(131072);

  //Run bnnconvolution : bnn_Conv_6__2
  int8_t* bnn_Conv_6__2_input_transpose = (int8_t *)VTABufferAlloc(16384);
  transpose_nhwc2vtaio_pack(Sign_5__1_res, (int8_t* )VTABufferGetVirtAddr(bnn_Conv_6__2_input_transpose), 1, 32, 32, 16);
  int8_t* bnn_Conv_6__2_output_bef_transpose = (int8_t *)VTABufferAlloc(131072);
  xp_convolution_wo_tr(bnn_Conv_6__2_input_transpose, layer1_1_conv1_weight__2, (int32_t *)layer1_1_conv1_bias__1, (int32_t *)layer1_1_conv1_bias__1, bnn_Conv_6__2_output_bef_transpose, 1, 32, 32, 16, 128, 3, 3, 1, 1, 0, 1, 7, 32, 32, vtaCmdH, 1, 8, 8, 0, 0);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(bnn_Conv_6__2_output_bef_transpose), bnn_Conv_6__2_res, 1, 32, 32, 128 );
  VTABufferFree(bnn_Conv_6__2_input_transpose);
  VTABufferFree(bnn_Conv_6__2_output_bef_transpose);

  //Run deallocactivation : dealloc_Sign_5__1_res
  free(Sign_5__1_res);

  //Run allocactivation : Sign_7__1_res
  int8_t *Sign_7__1_res = (int8_t *)malloc(131072);

  //Run elementsign : Sign_7__1
  elemsign(bnn_Conv_6__2_res, Sign_7__1_res, 131072);
 
  //Run deallocactivation : dealloc_bnn_Conv_6__2_res
  free(bnn_Conv_6__2_res);

  //Run allocactivation : bnn_Conv_8__2_res
  int8_t *bnn_Conv_8__2_res = (int8_t *)malloc(131072);

  //Run bnnconvolution : bnn_Conv_8__2
  int8_t* bnn_Conv_8__2_input_transpose = (int8_t *)VTABufferAlloc(16384);
  transpose_nhwc2vtaio_pack(Sign_7__1_res, (int8_t* )VTABufferGetVirtAddr(bnn_Conv_8__2_input_transpose), 1, 32, 32, 16);
  int8_t* bnn_Conv_8__2_output_bef_transpose = (int8_t *)VTABufferAlloc(131072);
  xp_convolution_wo_tr(bnn_Conv_8__2_input_transpose, layer1_1_conv2_weight__2, (int32_t *)layer1_1_conv2_bias__1, (int32_t *)layer1_1_conv2_bias__1, bnn_Conv_8__2_output_bef_transpose, 1, 32, 32, 16, 128, 3, 3, 1, 1, 0, 1, 8, 32, 32, vtaCmdH, 1, 8, 8, 0, 0);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(bnn_Conv_8__2_output_bef_transpose), bnn_Conv_8__2_res, 1, 32, 32, 128 );
  VTABufferFree(bnn_Conv_8__2_input_transpose);
  VTABufferFree(bnn_Conv_8__2_output_bef_transpose);

  //Run deallocactivation : dealloc_Sign_7__1_res
  free(Sign_7__1_res);

  //Run allocactivation : Sign_9__1_res
  int8_t *Sign_9__1_res = (int8_t *)malloc(131072);

  //Run elementsign : Sign_9__1
  elemsign(bnn_Conv_8__2_res, Sign_9__1_res, 131072);
 
  //Run deallocactivation : dealloc_bnn_Conv_8__2_res
  free(bnn_Conv_8__2_res);

  //Run allocactivation : bnn_Conv_10__2_res
  int8_t *bnn_Conv_10__2_res = (int8_t *)malloc(65536);

  //Run bnnconvolution : bnn_Conv_10__2
  int8_t* bnn_Conv_10__2_input_transpose = (int8_t *)VTABufferAlloc(16384);
  transpose_nhwc2vtaio_pack(Sign_9__1_res, (int8_t* )VTABufferGetVirtAddr(bnn_Conv_10__2_input_transpose), 1, 32, 32, 16);
  int8_t* bnn_Conv_10__2_output_bef_transpose = (int8_t *)VTABufferAlloc(65536);
  xp_convolution_wo_tr(bnn_Conv_10__2_input_transpose, layer2_0_conv1_weight__2, (int32_t *)layer2_0_conv1_bias__1, (int32_t *)layer2_0_conv1_bias__1, bnn_Conv_10__2_output_bef_transpose, 1, 32, 32, 16, 256, 3, 3, 1, 2, 0, 1, 7, 16, 16, vtaCmdH, 1, 8, 8, 0, 0);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(bnn_Conv_10__2_output_bef_transpose), bnn_Conv_10__2_res, 1, 16, 16, 256 );
  VTABufferFree(bnn_Conv_10__2_input_transpose);
  VTABufferFree(bnn_Conv_10__2_output_bef_transpose);

  //Run deallocactivation : dealloc_Sign_9__1_res
  free(Sign_9__1_res);

  //Run allocactivation : Sign_11__1_res
  int8_t *Sign_11__1_res = (int8_t *)malloc(65536);

  //Run elementsign : Sign_11__1
  elemsign(bnn_Conv_10__2_res, Sign_11__1_res, 65536);
 
  //Run deallocactivation : dealloc_bnn_Conv_10__2_res
  free(bnn_Conv_10__2_res);

  //Run allocactivation : bnn_Conv_12__2_res
  int8_t *bnn_Conv_12__2_res = (int8_t *)malloc(65536);

  //Run bnnconvolution : bnn_Conv_12__2
  int8_t* bnn_Conv_12__2_input_transpose = (int8_t *)VTABufferAlloc(8192);
  transpose_nhwc2vtaio_pack(Sign_11__1_res, (int8_t* )VTABufferGetVirtAddr(bnn_Conv_12__2_input_transpose), 1, 16, 16, 32);
  int8_t* bnn_Conv_12__2_output_bef_transpose = (int8_t *)VTABufferAlloc(65536);
  xp_convolution_wo_tr(bnn_Conv_12__2_input_transpose, layer2_0_conv2_weight__2, (int32_t *)layer2_0_conv2_bias__1, (int32_t *)layer2_0_conv2_bias__1, bnn_Conv_12__2_output_bef_transpose, 1, 16, 16, 32, 256, 3, 3, 1, 1, 0, 1, 8, 16, 16, vtaCmdH, 1, 8, 8, 0, 0);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(bnn_Conv_12__2_output_bef_transpose), bnn_Conv_12__2_res, 1, 16, 16, 256 );
  VTABufferFree(bnn_Conv_12__2_input_transpose);
  VTABufferFree(bnn_Conv_12__2_output_bef_transpose);

  //Run deallocactivation : dealloc_Sign_11__1_res
  free(Sign_11__1_res);

  //Run allocactivation : Sign_13__1_res
  int8_t *Sign_13__1_res = (int8_t *)malloc(65536);

  //Run elementsign : Sign_13__1
  elemsign(bnn_Conv_12__2_res, Sign_13__1_res, 65536);
 
  //Run deallocactivation : dealloc_bnn_Conv_12__2_res
  free(bnn_Conv_12__2_res);

  //Run allocactivation : bnn_Conv_14__2_res
  int8_t *bnn_Conv_14__2_res = (int8_t *)malloc(65536);

  //Run bnnconvolution : bnn_Conv_14__2
  int8_t* bnn_Conv_14__2_input_transpose = (int8_t *)VTABufferAlloc(8192);
  transpose_nhwc2vtaio_pack(Sign_13__1_res, (int8_t* )VTABufferGetVirtAddr(bnn_Conv_14__2_input_transpose), 1, 16, 16, 32);
  int8_t* bnn_Conv_14__2_output_bef_transpose = (int8_t *)VTABufferAlloc(65536);
  xp_convolution_wo_tr(bnn_Conv_14__2_input_transpose, layer2_1_conv1_weight__2, (int32_t *)layer2_1_conv1_bias__1, (int32_t *)layer2_1_conv1_bias__1, bnn_Conv_14__2_output_bef_transpose, 1, 16, 16, 32, 256, 3, 3, 1, 1, 0, 1, 13, 16, 16, vtaCmdH, 1, 8, 8, 0, 0);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(bnn_Conv_14__2_output_bef_transpose), bnn_Conv_14__2_res, 1, 16, 16, 256 );
  VTABufferFree(bnn_Conv_14__2_input_transpose);
  VTABufferFree(bnn_Conv_14__2_output_bef_transpose);

  //Run deallocactivation : dealloc_Sign_13__1_res
  free(Sign_13__1_res);

  //Run allocactivation : Sign_15__1_res
  int8_t *Sign_15__1_res = (int8_t *)malloc(65536);

  //Run elementsign : Sign_15__1
  elemsign(bnn_Conv_14__2_res, Sign_15__1_res, 65536);
 
  //Run deallocactivation : dealloc_bnn_Conv_14__2_res
  free(bnn_Conv_14__2_res);

  //Run allocactivation : bnn_Conv_16__2_res
  int8_t *bnn_Conv_16__2_res = (int8_t *)malloc(65536);

  //Run bnnconvolution : bnn_Conv_16__2
  int8_t* bnn_Conv_16__2_input_transpose = (int8_t *)VTABufferAlloc(8192);
  transpose_nhwc2vtaio_pack(Sign_15__1_res, (int8_t* )VTABufferGetVirtAddr(bnn_Conv_16__2_input_transpose), 1, 16, 16, 32);
  int8_t* bnn_Conv_16__2_output_bef_transpose = (int8_t *)VTABufferAlloc(65536);
  xp_convolution_wo_tr(bnn_Conv_16__2_input_transpose, layer2_1_conv2_weight__2, (int32_t *)layer2_1_conv2_bias__1, (int32_t *)layer2_1_conv2_bias__1, bnn_Conv_16__2_output_bef_transpose, 1, 16, 16, 32, 256, 3, 3, 1, 1, 0, 1, 9, 16, 16, vtaCmdH, 1, 8, 8, 0, 0);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(bnn_Conv_16__2_output_bef_transpose), bnn_Conv_16__2_res, 1, 16, 16, 256 );
  VTABufferFree(bnn_Conv_16__2_input_transpose);
  VTABufferFree(bnn_Conv_16__2_output_bef_transpose);

  //Run deallocactivation : dealloc_Sign_15__1_res
  free(Sign_15__1_res);

  //Run allocactivation : Sign_17__1_res
  int8_t *Sign_17__1_res = (int8_t *)malloc(65536);

  //Run elementsign : Sign_17__1
  elemsign(bnn_Conv_16__2_res, Sign_17__1_res, 65536);
 
  //Run deallocactivation : dealloc_bnn_Conv_16__2_res
  free(bnn_Conv_16__2_res);

  //Run allocactivation : bnn_Conv_18__2_res
  int8_t *bnn_Conv_18__2_res = (int8_t *)malloc(32768);

  //Run bnnconvolution : bnn_Conv_18__2
  int8_t* bnn_Conv_18__2_input_transpose = (int8_t *)VTABufferAlloc(8192);
  transpose_nhwc2vtaio_pack(Sign_17__1_res, (int8_t* )VTABufferGetVirtAddr(bnn_Conv_18__2_input_transpose), 1, 16, 16, 32);
  int8_t* bnn_Conv_18__2_output_bef_transpose = (int8_t *)VTABufferAlloc(32768);
  xp_convolution_wo_tr(bnn_Conv_18__2_input_transpose, layer3_0_conv1_weight__2, (int32_t *)layer3_0_conv1_bias__1, (int32_t *)layer3_0_conv1_bias__1, bnn_Conv_18__2_output_bef_transpose, 1, 16, 16, 32, 512, 3, 3, 1, 2, 0, 1, 12, 8, 8, vtaCmdH, 1, 4, 4, 0, 0);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(bnn_Conv_18__2_output_bef_transpose), bnn_Conv_18__2_res, 1, 8, 8, 512 );
  VTABufferFree(bnn_Conv_18__2_input_transpose);
  VTABufferFree(bnn_Conv_18__2_output_bef_transpose);

  //Run deallocactivation : dealloc_Sign_17__1_res
  free(Sign_17__1_res);

  //Run allocactivation : Sign_19__1_res
  int8_t *Sign_19__1_res = (int8_t *)malloc(32768);

  //Run elementsign : Sign_19__1
  elemsign(bnn_Conv_18__2_res, Sign_19__1_res, 32768);
 
  //Run deallocactivation : dealloc_bnn_Conv_18__2_res
  free(bnn_Conv_18__2_res);

  //Run allocactivation : bnn_Conv_20__2_res
  int8_t *bnn_Conv_20__2_res = (int8_t *)malloc(32768);

  //Run bnnconvolution : bnn_Conv_20__2
  int8_t* bnn_Conv_20__2_input_transpose = (int8_t *)VTABufferAlloc(4096);
  transpose_nhwc2vtaio_pack(Sign_19__1_res, (int8_t* )VTABufferGetVirtAddr(bnn_Conv_20__2_input_transpose), 1, 8, 8, 64);
  int8_t* bnn_Conv_20__2_output_bef_transpose = (int8_t *)VTABufferAlloc(32768);
  xp_convolution_wo_tr(bnn_Conv_20__2_input_transpose, layer3_0_conv2_weight__2, (int32_t *)layer3_0_conv2_bias__1, (int32_t *)layer3_0_conv2_bias__1, bnn_Conv_20__2_output_bef_transpose, 1, 8, 8, 64, 512, 3, 3, 1, 1, 0, 1, 15, 8, 8, vtaCmdH, 1, 4, 4, 0, 0);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(bnn_Conv_20__2_output_bef_transpose), bnn_Conv_20__2_res, 1, 8, 8, 512 );
  VTABufferFree(bnn_Conv_20__2_input_transpose);
  VTABufferFree(bnn_Conv_20__2_output_bef_transpose);

  //Run deallocactivation : dealloc_Sign_19__1_res
  free(Sign_19__1_res);

  //Run allocactivation : Sign_21__1_res
  int8_t *Sign_21__1_res = (int8_t *)malloc(32768);

  //Run elementsign : Sign_21__1
  elemsign(bnn_Conv_20__2_res, Sign_21__1_res, 32768);
 
  //Run deallocactivation : dealloc_bnn_Conv_20__2_res
  free(bnn_Conv_20__2_res);

  //Run allocactivation : bnn_Conv_22__2_res
  int8_t *bnn_Conv_22__2_res = (int8_t *)malloc(32768);

  //Run bnnconvolution : bnn_Conv_22__2
  int8_t* bnn_Conv_22__2_input_transpose = (int8_t *)VTABufferAlloc(4096);
  transpose_nhwc2vtaio_pack(Sign_21__1_res, (int8_t* )VTABufferGetVirtAddr(bnn_Conv_22__2_input_transpose), 1, 8, 8, 64);
  int8_t* bnn_Conv_22__2_output_bef_transpose = (int8_t *)VTABufferAlloc(32768);
  xp_convolution_wo_tr(bnn_Conv_22__2_input_transpose, layer3_1_conv1_weight__2, (int32_t *)layer3_1_conv1_bias__1, (int32_t *)layer3_1_conv1_bias__1, bnn_Conv_22__2_output_bef_transpose, 1, 8, 8, 64, 512, 3, 3, 1, 1, 0, 1, 16, 8, 8, vtaCmdH, 1, 4, 4, 0, 0);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(bnn_Conv_22__2_output_bef_transpose), bnn_Conv_22__2_res, 1, 8, 8, 512 );
  VTABufferFree(bnn_Conv_22__2_input_transpose);
  VTABufferFree(bnn_Conv_22__2_output_bef_transpose);

  //Run deallocactivation : dealloc_Sign_21__1_res
  free(Sign_21__1_res);

  //Run allocactivation : Sign_23__1_res
  int8_t *Sign_23__1_res = (int8_t *)malloc(32768);

  //Run elementsign : Sign_23__1
  elemsign(bnn_Conv_22__2_res, Sign_23__1_res, 32768);
 
  //Run deallocactivation : dealloc_bnn_Conv_22__2_res
  free(bnn_Conv_22__2_res);

  //Run allocactivation : bnn_Conv_24__2_res
  int8_t *bnn_Conv_24__2_res = (int8_t *)malloc(32768);

  //Run bnnconvolution : bnn_Conv_24__2
  int8_t* bnn_Conv_24__2_input_transpose = (int8_t *)VTABufferAlloc(4096);
  transpose_nhwc2vtaio_pack(Sign_23__1_res, (int8_t* )VTABufferGetVirtAddr(bnn_Conv_24__2_input_transpose), 1, 8, 8, 64);
  int8_t* bnn_Conv_24__2_output_bef_transpose = (int8_t *)VTABufferAlloc(32768);
  xp_convolution_wo_tr(bnn_Conv_24__2_input_transpose, layer3_1_conv2_weight__2, (int32_t *)layer3_1_conv2_bias__1, (int32_t *)layer3_1_conv2_bias__1, bnn_Conv_24__2_output_bef_transpose, 1, 8, 8, 64, 512, 3, 3, 1, 1, 0, 1, 11, 8, 8, vtaCmdH, 1, 4, 4, 0, 0);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(bnn_Conv_24__2_output_bef_transpose), bnn_Conv_24__2_res, 1, 8, 8, 512 );
  VTABufferFree(bnn_Conv_24__2_input_transpose);
  VTABufferFree(bnn_Conv_24__2_output_bef_transpose);

  //Run deallocactivation : dealloc_Sign_23__1_res
  free(Sign_23__1_res);

  //Run allocactivation : AveragePool_26__1_res
  int8_t *AveragePool_26__1_res = (int8_t *)malloc(512);

  //Run avgpool : AveragePool_26__1
  avgpool(bnn_Conv_24__2_res, 1.0/0.031250, 0, AveragePool_26__1_res, 1.0/0.031250, 0, 1, 8, 8, 512, 1, 1, 1, 512, 8, 8, 0, 8 );

  //Run deallocactivation : dealloc_bnn_Conv_24__2_res
  free(bnn_Conv_24__2_res);

  //Run allocactivation : Sign_33__1_res
  int8_t *Sign_33__1_res = (int8_t *)malloc(512);

  //Run elementsign : Sign_33__1
  elemsign(AveragePool_26__1_res, Sign_33__1_res, 512);
 
  //Run deallocactivation : dealloc_AveragePool_26__1_res
  free(AveragePool_26__1_res);

  //Run tensorview : Sign_33__1_res__2
  int8_t* Sign_33__1_res__2 = Sign_33__1_res;

  //Run allocactivation : Gemm_34__1_res
  int8_t *Gemm_34__1_res = (int8_t *)malloc(10);

  //Run fullyconnected : Gemm_34__1
  fullyconnected(Sign_33__1_res__2, 1.0/1.000000, 0, (int8_t *)VTABufferGetVirtAddr(fc_weight__1), 1.0/64.000000, 0, (int8_t *)VTABufferGetVirtAddr(fc_bias), 1.0/64.000000, 0, Gemm_34__1_res, 1.0/0.500000, 0, 1, 512, 512, 10, 1, 10, 1 );

  //Run deallocactivation : dealloc_Sign_33__1_res
  free(Sign_33__1_res);

  //Run dequantize : Gemm_34__1_dequantize
  int8_t* output = (int8_t*)mutableWeight + 12288;
  dequantize(Gemm_34__1_res, output, 10, 1/0.500000, 0 );

  //Run deallocactivation : dealloc_Gemm_34__1_res
  free(Gemm_34__1_res);
  return 0;
}