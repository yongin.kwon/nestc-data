

#include "vta/runtime.h"
#include "VTABundle.h"
#include <time.h>
#include <iostream>
#include <fstream>
#include "Conv_inception_5b_5x5_1__1MainEntry.h"
SymbolTableEntry symbolTableEntry_Conv_inception_5b_5x5_1__1Bundle[2]={{"inputP",0,2352,'1'},{"outP",2352,6272,'1'}};
BundleConfig Conv_inception_5b_5x5_1__1Bundle_config = {154112, 8624, 0, 64, 2, symbolTableEntry_Conv_inception_5b_5x5_1__1Bundle};
namespace namespace_Conv_inception_5b_5x5_1__1Bundle {
int8_t* filterP;
int8_t* biasP;
}
using namespace namespace_Conv_inception_5b_5x5_1__1Bundle;
extern VTACommandHandle vtaCmdH;

void Conv_inception_5b_5x5_1__1MainEntry_load_module(uint8_t *constantWeight){
  filterP = (int8_t *)VTABufferAlloc(153600);
  VTABufferCopy((int8_t *)(constantWeight + 0), 0, filterP, 0, 153600, 1);
  biasP = (int8_t *)VTABufferAlloc(512);
  VTABufferCopy((int8_t *)(constantWeight + 153600), 0, biasP, 0, 512, 1);
}

void Conv_inception_5b_5x5_1__1MainEntry_destroy_module(){
  VTABufferFree(filterP);
  VTABufferFree(biasP);
}
int Conv_inception_5b_5x5_1__1MainEntry(uint8_t *constantWeight, uint8_t *mutableWeight, uint8_t *activations){

  //Run convolution : conv
  int8_t* inputP = (int8_t*)mutableWeight + 0;
  int8_t* outP = (int8_t*)mutableWeight + 2352;
  int8_t* conv_input_transpose = (int8_t *)VTABufferAlloc(2352);
  transpose_nhwc2vtaio(inputP, (int8_t* )VTABufferGetVirtAddr(conv_input_transpose), 1, 7, 7, 48);
  int8_t* conv_output_bef_transpose = (int8_t *)VTABufferAlloc(6272);
  convolution_wo_tr(conv_input_transpose, filterP, (int32_t *)biasP, conv_output_bef_transpose, 1, 7, 7, 48, 128, 5, 5, 2, 1, 0, 1, 9, 7, 7, vtaCmdH, 1, 14, 14, 0);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(conv_output_bef_transpose), outP, 1, 7, 7, 128 );
  VTABufferFree(conv_input_transpose);
  VTABufferFree(conv_output_bef_transpose);
  return 0;
}