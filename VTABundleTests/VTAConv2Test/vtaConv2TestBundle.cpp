

#include "vta/runtime.h"
#include "VTABundle.h"
#include <time.h>
#include <iostream>
#include <fstream>
#include "vtaConv2TestMainEntry.h"
SymbolTableEntry symbolTableEntry_vtaConv2TestBundle[2]={{"inputP",0,6272,'1'},{"outP",6272,6272,'1'}};
BundleConfig vtaConv2TestBundle_config = {9344, 12544, 0, 64, 2, symbolTableEntry_vtaConv2TestBundle};
int8_t* filterP;
int8_t* biasP;
extern VTACommandHandle vtaCmdH;

void vtaConv2TestMainEntry_load_module(uint8_t *constantWeight){
  filterP = (int8_t *)VTABufferAlloc(9216);
  VTABufferCopy((int8_t *)(constantWeight + 0), 0, filterP, 0, 9216, 1);
  biasP = (int8_t *)VTABufferAlloc(128);
  VTABufferCopy((int8_t *)(constantWeight + 9216), 0, biasP, 0, 128, 1);
}

void vtaConv2TestMainEntry_destroy_module(){
  VTABufferFree(filterP);
  VTABufferFree(biasP);
}
int vtaConv2TestMainEntry(uint8_t *constantWeight, uint8_t *mutableWeight, uint8_t *activations){

  //Run convolution : conv
  int8_t* inputP = (int8_t*)mutableWeight + 0;
  int8_t* outP = (int8_t*)mutableWeight + 6272;
  int8_t* conv_input_transpose = (int8_t *)VTABufferAlloc(6272);
  transpose_nhwc2vtaio(inputP, (int8_t* )VTABufferGetVirtAddr(conv_input_transpose), 1, 14, 14, 32);
  int8_t* conv_output_bef_transpose = (int8_t *)VTABufferAlloc(6272);
  convolution_wo_tr(conv_input_transpose, filterP, (int32_t *)biasP, conv_output_bef_transpose, 1, 14, 14, 32, 32, 3, 3, 1, 1, 0, 1, 6, 14, 14, vtaCmdH);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(conv_output_bef_transpose), outP, 1, 14, 14, 32 );
  VTABufferFree(conv_input_transpose);
  VTABufferFree(conv_output_bef_transpose);
  return 0;
}