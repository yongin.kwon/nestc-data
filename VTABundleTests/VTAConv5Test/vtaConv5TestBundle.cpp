

#include "vta/runtime.h"
#include "VTABundle.h"
#include <time.h>
#include <iostream>
#include <fstream>
#include "vtaConv5TestMainEntry.h"
SymbolTableEntry symbolTableEntry_vtaConv5TestBundle[2]={{"inputP",0,200704,'1'},{"outP",200704,100352,'1'}};
BundleConfig vtaConv5TestBundle_config = {8704, 301056, 0, 64, 2, symbolTableEntry_vtaConv5TestBundle};
int8_t* filterP;
int8_t* biasP;
extern VTACommandHandle vtaCmdH;

void vtaConv5TestMainEntry_load_module(uint8_t *constantWeight){
  filterP = (int8_t *)VTABufferAlloc(8192);
  VTABufferCopy((int8_t *)(constantWeight + 0), 0, filterP, 0, 8192, 1);
  biasP = (int8_t *)VTABufferAlloc(512);
  VTABufferCopy((int8_t *)(constantWeight + 8192), 0, biasP, 0, 512, 1);
}

void vtaConv5TestMainEntry_destroy_module(){
  VTABufferFree(filterP);
  VTABufferFree(biasP);
}
int vtaConv5TestMainEntry(uint8_t *constantWeight, uint8_t *mutableWeight, uint8_t *activations){

  //Run convolution : conv
  int8_t* inputP = (int8_t*)mutableWeight + 0;
  int8_t* outP = (int8_t*)mutableWeight + 200704;
  int8_t* conv_input_transpose = (int8_t *)VTABufferAlloc(200704);
  transpose_nhwc2vtaio(inputP, (int8_t* )VTABufferGetVirtAddr(conv_input_transpose), 1, 56, 56, 64);
  int8_t* conv_output_bef_transpose = (int8_t *)VTABufferAlloc(100352);
  convolution_wo_tr(conv_input_transpose, filterP, (int32_t *)biasP, conv_output_bef_transpose, 1, 56, 56, 64, 128, 1, 1, 0, 2, 0, 1, 6, 28, 28, vtaCmdH, 2, 7, 28);
  transpose_vtaio2nhwc((int8_t* )VTABufferGetVirtAddr(conv_output_bef_transpose), outP, 1, 28, 28, 128 );
  VTABufferFree(conv_input_transpose);
  VTABufferFree(conv_output_bef_transpose);
  return 0;
}