

#include "vta/runtime.h"
#include "VTABundle.h"
#include <time.h>
#include <iostream>
#include <fstream>
#include "vtaAvgPoolTestMainEntry.h"
SymbolTableEntry symbolTableEntry_vtaAvgPoolTestBundle[2]={{"inputP",0,100352,'1'},{"outP",100352,2048,'1'}};
BundleConfig vtaAvgPoolTestBundle_config = {0, 102400, 0, 64, 2, symbolTableEntry_vtaAvgPoolTestBundle};
extern VTACommandHandle vtaCmdH;

void vtaAvgPoolTestMainEntry_load_module(uint8_t *constantWeight){
}

void vtaAvgPoolTestMainEntry_destroy_module(){
}
int vtaAvgPoolTestMainEntry(uint8_t *constantWeight, uint8_t *mutableWeight, uint8_t *activations){

  //Run avgpool : avgPool
  int8_t* inputP = (int8_t*)mutableWeight + 0;
  int8_t* outP = (int8_t*)mutableWeight + 100352;
  avgpool(inputP, 1.0/1.000000, 0, outP, 1.0/8.000000, 0, 1, 7, 7, 2048, 1, 1, 1, 2048, 7, 7, 0, 1 );
  return 0;
}