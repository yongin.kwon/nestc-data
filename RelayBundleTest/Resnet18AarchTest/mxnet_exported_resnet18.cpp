// Generate Include Header
#include "mxnet_exported_resnet18.h"
#include <dlpack/dlpack.h>
#include <tvm/runtime/module.h>
#include <tvm/runtime/packed_func.h>
#include <tvm/runtime/registry.h>
#include <cstdio>
#include <assert.h>
SymbolTableEntry symbolTableEntry_mxnet_exported_resnet18[2]={{"data",0,150528,'1'},{"resnetv10_dense0_fwd__1",602112,1000,'1'}};
struct BundleConfig mxnet_exported_resnet18_config = {
    47943072,
    606112,
    0, 64,
    2,
    symbolTableEntry_mxnet_exported_resnet18
};
DLTensor input_0;
DLTensor output_602112;
tvm::runtime::Module gmod;
tvm::runtime::PackedFunc set_input;
tvm::runtime::PackedFunc get_output;
tvm::runtime::PackedFunc run;
int mxnet_exported_resnet18_load_module(uint8_t* constantWeight) {
  DLDevice dev{kDLCPU, 0};
  tvm::runtime::Module mod_factory = tvm::runtime::Module::LoadFromFile("./mxnet_exported_resnet18_tvm.so");
  gmod = mod_factory.GetFunction("default")(dev);
  set_input = gmod.GetFunction("set_input");
  get_output = gmod.GetFunction("get_output");
  run = gmod.GetFunction("run");
  return 0;
}
int mxnet_exported_resnet18(uint8_t* constantWeight, uint8_t *mutableWeight, uint8_t *activations) {
  uint8_t* data = (uint8_t*)mutableWeight + 0;
  std::vector<int64_t> input_shape_0 = {1,224,224,3 };
  input_0.data = data;
  input_0.device = DLDevice{kDLCPU, 0};
  input_0.ndim = 4;
  input_0.dtype = DLDataType{kDLFloat, 32, 1};
  input_0.shape = input_shape_0.data();
  input_0.strides = nullptr;
  input_0.byte_offset = 0;
  uint8_t* resnetv10_dense0_fwd__1 = (uint8_t*)mutableWeight + 602112;
  std::vector<int64_t> output_shape_602112 = {1,1000 };
  output_602112.data = resnetv10_dense0_fwd__1;
  output_602112.device = DLDevice{kDLCPU, 0};
  output_602112.ndim = 2;
  output_602112.dtype = DLDataType{kDLFloat, 32, 1};
  output_602112.shape = output_shape_602112.data();
  output_602112.strides = nullptr;
  output_602112.byte_offset = 0;
  set_input("data", &input_0);
  run();
  get_output(0, &output_602112);
  return 0;
}
int mxnet_exported_resnet18_destory_module() {
  return 0;
}
